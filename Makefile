
-include .make/base.mk

-include .make/oci.mk

-include PrivateRules.mak

# Image URL to use all building/pushing image targets
IMG_PREFIX ?= registry.gitlab.com/piersharding/elktail-amd64
IMG ?= $(IMG_PREFIX):$(VERSION)

# OCI_BUILDER = podman
CAR_OCI_REGISTRY_HOST = registry.gitlab.com/piersharding

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif
ELKTAIL_CMD_DIR ?= cmd/elktail
ELKTAIL_GO_SOURCE=$(ELKTAIL_CMD_DIR)/main.go
ELKUSER_CMD_DIR ?= cmd/elkuser
ELKUSER_GO_SOURCE=$(ELKUSER_CMD_DIR)/main.go

# Setting SHELL to bash allows bash commands to be executed by recipes.
# This is a requirement for 'setup-envtest.sh' in the test target.
# Options are set to exit when a recipe line exits non-zero or a piped command fails.
SHELL = /usr/bin/env bash -o pipefail
.SHELLFLAGS = -ec

all: build

##@ General
tidy:
	go get -u ./...
	go mod tidy

fmt: ## Run go fmt against code.
	go fmt ./...

vet: ## Run go vet against code.
	pwd
	env | grep GO
	ls -latr *
	go mod download
	go vet ./...

test: build ## Run tests.
	go test ./... -coverprofile cover.out || true

clean:
	rm -f bin/*
	cd images/elktail-amd64 && git clean -f .
	cd images/elktail-arm64 && git clean -f .

build: fmt vet clean ## Build manager binary.

	mkdir -p bin
	export GOOS=linux GOARCH=amd64 && go build -o bin/elktail-$${GOOS}-$${GOARCH}-$(VERSION) $(ELKTAIL_GO_SOURCE)
	export GOOS=linux GOARCH=arm64 && go build -o bin/elktail-$${GOOS}-$${GOARCH}-$(VERSION) $(ELKTAIL_GO_SOURCE)
	export GOOS=windows GOARCH=amd64 && go build -o bin/elktail-$${GOOS}-$${GOARCH}-$(VERSION).exe $(ELKTAIL_GO_SOURCE)
	export GOOS=darwin GOARCH=amd64 && go build -o bin/elktail-$${GOOS}-$${GOARCH}-$(VERSION) $(ELKTAIL_GO_SOURCE)
	export GOOS=darwin GOARCH=arm64 && go build -o bin/elktail-$${GOOS}-$${GOARCH}-$(VERSION) $(ELKTAIL_GO_SOURCE)
	export GOOS=linux GOARCH=amd64 && go build -o bin/elkuser-$${GOOS}-$${GOARCH}-$(VERSION) $(ELKUSER_GO_SOURCE)
	export GOOS=linux GOARCH=arm64 && go build -o bin/elkuser-$${GOOS}-$${GOARCH}-$(VERSION) $(ELKUSER_GO_SOURCE)
	export GOOS=windows GOARCH=amd64 && go build -o bin/elkuser-$${GOOS}-$${GOARCH}-$(VERSION).exe $(ELKUSER_GO_SOURCE)
	export GOOS=darwin GOARCH=amd64 && go build -o bin/elkuser-$${GOOS}-$${GOARCH}-$(VERSION) $(ELKUSER_GO_SOURCE)
	export GOOS=darwin GOARCH=arm64 && go build -o bin/elkuser-$${GOOS}-$${GOARCH}-$(VERSION) $(ELKUSER_GO_SOURCE)
	ls -latr bin/*

build-oci: ## Build both images
	make oci-build OCI_IMAGE=$(PROJECT_NAME)-amd64 OCI_IMAGE_BUILD_CONTEXT=./
	$(OCI_BUILDER) tag $(CAR_OCI_REGISTRY_HOST)/$(PROJECT_NAME)-amd64:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	   $(IMG_PREFIX)-amd64:$(VERSION)
	make oci-build OCI_IMAGE=$(PROJECT_NAME)-arm64 OCI_IMAGE_BUILD_CONTEXT=./
	$(OCI_BUILDER) tag $(CAR_OCI_REGISTRY_HOST)/$(PROJECT_NAME)-arm64:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	   $(IMG_PREFIX)-arm64:$(VERSION)
	$(OCI_BUILDER) manifest rm $(IMG_PREFIX):$(VERSION) || true
	# $(OCI_BUILDER) manifest create $(IMG_PREFIX):$(VERSION) $(IMG_PREFIX)-amd64:$(VERSION) $(IMG_PREFIX)-arm64:$(VERSION)
	# $(OCI_BUILDER) manifest add $(IMG_PREFIX):$(VERSION) $(IMG_PREFIX)-amd64:$(VERSION)
	# $(OCI_BUILDER) manifest add $(IMG_PREFIX):$(VERSION) $(IMG_PREFIX)-arm64:$(VERSION)

oci-pre-build:
	cd images/elktail-amd64 && \
	cp -r ../../go.mod . && \
	cp -r ../../go.sum . && \
	cp -r ../../actions/ . && \
	cp -r ../../cmd/ . && \
	cp -r ../../color/ . && \
	cp -r ../../configuration/ . && \
	cp -r ../../logging/ . && \
	cp -r ../../req/ . && \
	cp -r ../../ssh/ . && \
	cp -r ../../tail/ . && \
	cp -r ../../version/ .

	cd images/elktail-arm64 && \
	cp -r ../../go.mod . && \
	cp -r ../../go.sum . && \
	cp -r ../../actions/ . && \
	cp -r ../../cmd/ . && \
	cp -r ../../color/ . && \
	cp -r ../../configuration/ . && \
	cp -r ../../logging/ . && \
	cp -r ../../req/ . && \
	cp -r ../../ssh/ . && \
	cp -r ../../tail/ . && \
	cp -r ../../version/ .

oci-post-build:
	cd images/elktail-amd64 && git clean -f .
	cd images/elktail-arm64 && git clean -f .


dotenv: ## set release variables - build release unless CI_COMMIT_TAG set
	@echo "RELEASE=$(VERSION)" > variables.env
	@echo "ELKTAIL_LINUX_AMD64_BINARY=elktail-linux-amd64-$(VERSION)" >> variables.env
	@echo "ELKTAIL_LINUX_ARM64_BINARY=elktail-linux-arm64-$(VERSION)" >> variables.env
	@echo "ELKTAIL_WINDOWS_AMD64_BINARY=elktail-windows-amd64-$(VERSION)" >> variables.env
	@echo "ELKTAIL_DARWIN_AMD64_BINARY=elktail-darwin-amd64-$(VERSION)" >> variables.env
	@echo "ELKTAIL_DARWIN_ARM64_BINARY=elktail-darwin-arm64-$(VERSION)" >> variables.env
	@echo "ELKUSER_LINUX_AMD64_BINARY=elkuser-linux-amd64-$(VERSION)" >> variables.env
	@echo "ELKUSER_LINUX_ARM64_BINARY=elkuser-linux-arm64-$(VERSION)" >> variables.env
	@echo "ELKUSER_WINDOWS_AMD64_BINARY=elkuser-windows-amd64-$(VERSION)" >> variables.env
	@echo "ELKUSER_DARWIN_AMD64_BINARY=elkuser-darwin-amd64-$(VERSION)" >> variables.env
	@echo "ELKUSER_DARWIN_ARM64_BINARY=elkuser-darwin-arm64-$(VERSION)" >> variables.env
	@if [ -z "$(CI_COMMIT_TAG)" ]; then \
	RELEASE_SUFFIX=".c${CI_COMMIT_SHORT_SHA}"; \
	else \
	RELEASE_SUFFIX=""; \
	fi; \
	echo "RELEASE_SUFFIX=$${RELEASE_SUFFIX}" >> variables.env
	@if [ -z "${CI_COMMIT_TAG}" ]; then \
	RELEASE_REF="${CI_COMMIT_SHORT_SHA}"; \
	else \
	RELEASE_REF="${CI_COMMIT_TAG}"; \
	fi; \
	echo "RELEASE_REF=$${RELEASE_REF}" >> variables.env
	cat variables.env

elktail-set-version:
	@sed -i.x -e "s/^const VERSION =.*/const VERSION = \"$(VERSION)\"/" version/version.go 2>/dev/null
	@rm -f version/version.go.x

go-patch: bump-patch-release elktail-set-version git-create-tag git-push-tag

go-minor: bump-minor-release elktail-set-version git-create-tag git-push-tag

go-major: bump-major-release elktail-set-version git-create-tag git-push-tag
