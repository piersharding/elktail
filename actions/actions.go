/* Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
package actions

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/urfave/cli/v2"
	"gitlab.com/piersharding/elktail/configuration"
	"gitlab.com/piersharding/elktail/logging"
	request "gitlab.com/piersharding/elktail/req"
)

var (
	Trace *log.Logger
	Info  *log.Logger
	Error *log.Logger
)

// current base compatibility
var BASE_ELASTICSEARCH_VERSION = "8.17.0"

// common setup for each command line execution
func setUp(c *cli.Context, skip bool) {

	if c.IsSet("help") {
		_ = cli.ShowAppHelp(c)
		os.Exit(0)
	}
	// setup logging
	config := configuration.Config

	if config.MoreVerbose || config.TraceRequests {
		logging.InitLogging(os.Stderr, os.Stderr, os.Stderr, true)
	} else if config.Verbose {
		logging.InitLogging(io.Discard, os.Stderr, os.Stderr, false)
	} else {
		logging.InitLogging(io.Discard, io.Discard, os.Stderr, false)
	}
	Trace = logging.Trace
	Info = logging.Info
	Error = logging.Error

	if config.MoreVerbose || config.TraceRequests {
		confJs, _ := json.MarshalIndent(config, "", "  ")
		Trace.Printf("Configuration BEFORE load (from commandline) is: %s", string(confJs))
	}

	var err error
	var loadedConfig *configuration.Configuration
	if c.IsSet("c") {
		loadedConfig, err = configuration.LoadDefault(config.ConfigFile)
	} else {
		loadedConfig, err = configuration.LoadDefault(configuration.ConfFile)
	}
	if err != nil {
		Info.Printf("Failed to find or open previous default configuration: %s\n", err)
	} else {
		loadedConfig.Copy(config, c)
		Info.Printf("Loaded previous config and connecting to host %s.\n", loadedConfig.SearchTarget.Url)

	}
	if config.MoreVerbose || config.TraceRequests {
		confJs, _ := json.MarshalIndent(config, "", "  ")
		Trace.Printf("Configuration AFTER load is: %s", string(confJs))
	}

	if config.User != "" {
		credentials := strings.Split(config.User, ":")
		config.User = credentials[0]
		if len(credentials) == 2 {
			config.Password = credentials[1]
		} else {
			if !skip {
				Error.Fatalf("Must provide a username:password for the authenticating user value")
			}
		}
	} else {
		if !skip {
			Error.Fatalf("Must provide a user value of username:password")
		}
	}

	if config.Output != "" {
		config.OutputStyle = "light"
		style := strings.Split(config.Output, "+")
		config.Output = style[0]
		if len(style) == 2 {
			config.OutputStyle = style[1]
		}
	}
	Trace.Printf("SetUp: config.Output/Style: %s / %s", config.Output, config.OutputStyle)
	Trace.Printf("Configuration before we go in is: %#v", config)

	client := request.BuildClient(config)

	// get server info
	sreq := request.BuildRequest(config, "", "", "GET")
	Trace.Printf("Req: %v", sreq)

	sresp, err := client.Do(sreq)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	if sresp.StatusCode == 401 {
		Error.Printf("Unable to authenticate user: %s.  Check username:password supplied", sresp.Status)
		_ = cli.ShowAppHelp(c)
		os.Exit(1)
	}

	sbody, err := io.ReadAll(sresp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", sresp)
	Trace.Printf("Server: %s", string(sbody))

	var server_config map[string]interface{}
	err = json.Unmarshal(sbody, &server_config)
	if err != nil {
		Trace.Printf("ERR [Unmarshal] %s", err)
		panic(err)
	}

	Trace.Printf("Obj: %v", server_config)
	version := server_config["version"].(map[string]interface{})["number"].(string)
	Trace.Printf("Server version: %v", version)
	config.ServerVersion = compareVer(BASE_ELASTICSEARCH_VERSION, version)

}

// compare semantic versions - for the ElasticSearch server compatibility
func compareVer(a, b string) (ret int) {
	as := strings.Split(a, ".")
	bs := strings.Split(b, ".")
	loopMax := len(bs)
	if len(as) > len(bs) {
		loopMax = len(as)
	}
	for i := 0; i < loopMax; i++ {
		var x, y string
		if len(as) > i {
			x = as[i]
		}
		if len(bs) > i {
			y = bs[i]
		}
		xi, _ := strconv.Atoi(x)
		yi, _ := strconv.Atoi(y)
		if xi > yi {
			ret = -1
		} else if xi < yi {
			ret = 1
		}
		if ret != 0 {
			break
		}
	}
	return
}

func Main(c *cli.Context) error {

	_ = cli.ShowAppHelp(c)
	os.Exit(1)

	return nil
}

var templateDir = ".elktail"
var defaultTemplateFile = "role-template.json"
var RoleTemplateFile string

// load the role template for user creation
func loadRoleTemplateFile(templateFile string) (roleTemplateStr string, err error) {
	var roleTemplate map[string]interface{}
	templateBytes, err := os.ReadFile(templateFile)
	if err != nil {
		return RolePermissionsDefault, err
	}
	if !strings.HasSuffix(templateFile, ".json") {
		return RolePermissionsDefault, fmt.Errorf("Role template file is not a JSON file: %s", templateFile)
	}
	err = json.Unmarshal(templateBytes, &roleTemplate)
	if err != nil {
		return RolePermissionsDefault, err
	}
	return string(templateBytes), nil
}

// setup the output renderer based on the requested type
func setOutputAndRender(t table.Writer, config *configuration.Configuration) {
	if config.Output == "csv" {
		t.SetCaption("")
		t.RenderCSV()
	} else if config.Output == "tsv" {
		t.SetCaption("")
		t.RenderTSV()
	} else {
		if config.OutputStyle == "bright" {
			t.SetStyle(table.StyleColoredBright)
		} else if config.OutputStyle == "default" {
			t.SetStyle(table.StyleDefault)
		} else {
			t.SetStyle(table.StyleLight)
		}
		t.Render()
	}

}

// generate some sort of pseudo random password n long
func genPassword(length int) string {
	// rand.Seed(time.Now().UnixNano())
	digits := "0123456789"
	specials := "~=+%^*/()[]{}/!@#$?|"
	chars := "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz"
	all := chars +
		digits + specials
	buf := make([]byte, length)
	// first char must be alpha
	buf[0] = chars[rand.Intn(len(chars))]
	// must have atleast 1 digit
	buf[1] = digits[rand.Intn(len(digits))]
	// must have atleast 1 special char
	buf[2] = specials[rand.Intn(len(specials))]
	for i := 3; i < length; i++ {
		buf[i] = all[rand.Intn(len(all))]
	}
	// only shuffle n+1 chars
	rand.Shuffle(len(buf)-1, func(i, j int) {
		buf[i+1], buf[j+1] = buf[j+1], buf[i+1]
	})
	return string(buf) // E.g. "a3i[g0|)z"
}

// Declare the commands and sub-commands for elkuser
func Commands(config *configuration.Configuration) []*cli.Command {
	// https://cli.urfave.org/v2/examples/subcommands/
	defaultRoles := []string{RoleNameDefault}
	roles := cli.NewStringSlice(defaultRoles...)
	// set the default config file name
	templateDirPath := configuration.UserHomeDir() + string(os.PathSeparator) + templateDir
	RoleTemplateFile = templateDirPath + string(os.PathSeparator) + defaultTemplateFile

	return []*cli.Command{
		{
			Name:    "role",
			Aliases: []string{"r"},
			Usage:   "Role management",
			Subcommands: []*cli.Command{
				{
					Name:     "list",
					Aliases:  []string{"l"},
					Category: "role",
					Usage:    "list roles: elkuser role list",
					Action:   RoleList,
					Flags: []cli.Flag{
						&cli.StringFlag{
							Name:        "output",
							EnvVars:     []string{"ELKUSER_OUTPUT"},
							Value:       "standard",
							Usage:       "Output style - standard+light(default can also be standard+bright, standard+default), csv, tsv",
							Destination: &config.Output,
							Category:    "ROLE LIST",
						},
					},
				},
				{

					Name:     "create",
					Aliases:  []string{"c"},
					Category: "role",
					Usage:    "create/update a role: elkuser role create <rolename>",
					Action:   RoleCreate,
					Flags: []cli.Flag{
						&cli.StringFlag{
							Name:        "roletemplate",
							EnvVars:     []string{"ELKUSER_ROLE_TEMPLATE"},
							Value:       RoleTemplateFile,
							Usage:       "Role template file - must be .json with role snippet - see var RolePermissionsDefault",
							Destination: &config.RoleTemplateFile,
							Category:    "ROLE CREATE",
						},
						&cli.BoolFlag{
							Name:        "confirm",
							Usage:       "Force overwrite existing role",
							Destination: &config.Confirm,
							Category:    "ROLE CREATE",
						},
					},
				},
				{
					Name:     "delete",
					Aliases:  []string{"d"},
					Category: "role",
					Usage:    "delete an existing role: elkuser role delete <rolename>",
					Action:   RoleDelete,
					Flags: []cli.Flag{
						&cli.BoolFlag{
							Name:        "confirm",
							Usage:       "Confirm delete of existing role",
							Destination: &config.Confirm,
							Category:    "ROLE DELETE",
						},
					},
				},
			},
		},
		{
			Name:    "user",
			Aliases: []string{"u"},
			Usage:   "User management",
			Subcommands: []*cli.Command{
				{
					Name:     "list",
					Aliases:  []string{"l"},
					Category: "user",
					Usage:    "list users: elkuser user list",
					Action:   UserList,
					Flags: []cli.Flag{
						&cli.StringFlag{
							Name:        "output",
							EnvVars:     []string{"ELKUSER_OUTPUT"},
							Value:       "standard",
							Usage:       "Output style - standard+light(default can also be standard+bright, standard+default), csv, tsv",
							Destination: &config.Output,
							Category:    "USER LIST",
						},
					},
				},
				{
					Name:     "create",
					Aliases:  []string{"c"},
					Category: "user",
					Usage:    "create a new user: elkuser user create <username> (<password>)",
					Action:   UserCreate,
					Flags: []cli.Flag{
						&cli.StringSliceFlag{
							Value:       roles,
							Name:        "role",
							Usage:       "Role to give to user.  Repeat to add multiple",
							Destination: &config.RoleNames,
							Category:    "USER CREATE",
						},
						&cli.StringFlag{
							Value:       "Nobody",
							Name:        "fullname",
							Usage:       "Full Name",
							Destination: &config.FullName,
							Category:    "USER CREATE",
						},
						&cli.StringFlag{
							Value:       "nobody@example.com",
							Name:        "email",
							Usage:       "Email address",
							Destination: &config.EmailAddress,
							Category:    "USER CREATE",
						},
						&cli.BoolFlag{
							Name:        "confirm",
							Usage:       "Confirm overwrite of existing user",
							Destination: &config.Confirm,
							Category:    "USER CREATE",
						},
					},
				},
				{
					Name:     "password",
					Aliases:  []string{"p"},
					Category: "user",
					Usage:    "Update user password: elkuser --user <old user:password> user password <username> <new-password>",
					Action:   UserChangePassword,
					Flags:    []cli.Flag{},
				},
				{
					Name:     "delete",
					Aliases:  []string{"d"},
					Category: "user",
					Usage:    "delete an existing user: elkuser user delete <username>",
					Action:   UserDelete,
					Flags: []cli.Flag{
						&cli.BoolFlag{
							Name:        "confirm",
							Usage:       "Confirm delete of existing user",
							Destination: &config.Confirm,
							Category:    "USER DELETE",
						},
					},
				},
			},
		},
		{
			Name:    "apikey",
			Aliases: []string{"t"},
			Usage:   "API Key management",
			Subcommands: []*cli.Command{
				{
					Name:     "list",
					Category: "apikey",
					Aliases:  []string{"l"},
					Usage:    "list apikeys: elkuser apikey list",
					Action:   APIKeyList,
					Flags: []cli.Flag{
						&cli.StringFlag{
							Name:        "output",
							EnvVars:     []string{"ELKUSER_OUTPUT"},
							Value:       "standard",
							Usage:       "Output style - standard+light(default can also be standard+bright, standard+default), csv, tsv",
							Destination: &config.Output,
							Category:    "USER LIST",
						},
					},
				},
				{
					Name:     "config",
					Category: "apikey",
					Usage:    "generate apikey config: elkuser apikey config (<apikey to include>)",
					Action:   APIKeyConfig,
					Flags: []cli.Flag{
						&cli.BoolFlag{
							Name:        "c",
							Aliases:     []string{"command-line"},
							Usage:       "Output the command line arguments",
							Destination: &config.Confirm,
							Category:    "APIKEY DELETE",
						},
					},
				},
				{

					Name:     "create",
					Category: "apikey",
					Aliases:  []string{"c"},
					Usage:    "create a new apikey for a given user: elkuser apikey create <username>",
					Action:   APIKeyCreate,
					Flags: []cli.Flag{
						&cli.StringFlag{
							Name:        "roletemplate",
							EnvVars:     []string{"ELKUSER_ROLE_TEMPLATE"},
							Value:       RoleTemplateFile,
							Usage:       "Role template file - must be .json with role snippet - see var RolePermissionsDefault",
							Destination: &config.RoleTemplateFile,
							Category:    "APIKEY CREATE",
						},
						&cli.StringFlag{
							Name:        "apikeyexpiry",
							EnvVars:     []string{"ELKUSER_ROLE_TEMPLATE"},
							Value:       "365d",
							Usage:       "API Key expiry time (default: 365d)",
							Destination: &config.APIKeyExpiryTime,
							Category:    "APIKEY CREATE",
						},
					},
				},
				{
					Name:     "delete",
					Category: "apikey",
					Aliases:  []string{"d"},
					Usage:    "delete an existing apikey: elkuser apikey delete <apikey id>",
					Action:   APIKeyDelete,
					Flags: []cli.Flag{
						&cli.BoolFlag{
							Name:        "confirm",
							Usage:       "Confirm delete of existing apikey",
							Destination: &config.Confirm,
							Category:    "APIKEY DELETE",
						},
					},
				},
			},
		},
	}

}
