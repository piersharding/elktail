/* Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
package actions

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"regexp"
	"slices"
	"strings"
	"time"

	"github.com/PaesslerAG/jsonpath"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/ledongthuc/goterators"
	"github.com/urfave/cli/v2"
	"gitlab.com/piersharding/elktail/configuration"
	request "gitlab.com/piersharding/elktail/req"
	"sigs.k8s.io/yaml"
)

func APIKeyList(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)

	client := request.BuildClient(config)

	// curl --cert config/ca/kibana-tls-signed.crt --key config/ca/kibana-tls.key --cacert config/ca/ca-bundle.pem \
	// -X GET "https://elastic:elastic_yolo@elasticsearch.local.net:9200/_security/api_key?pretty" -H 'Content-Type: application/json'

	// request for listing all tokens - you might have super rights?
	req := request.BuildRequest(config, "/_security/api_key?active_only=true&pretty", "", "GET")
	Trace.Printf("Req: %v", req)

	resp, err := client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	if resp.StatusCode != 200 {
		// active_only or super rights may not be available in this version of ElasticSearch
		req = request.BuildRequest(config, "/_security/api_key?owner=true&active_only=true&pretty", "", "GET")
		Trace.Printf("Req: %v", req)

		resp, err = client.Do(req)

		if err != nil {
			Error.Fatalf("http.Request failed: %s", err)
		}
		if resp.StatusCode == 403 {
			body, _ := io.ReadAll(resp.Body)
			Trace.Printf("Insufficient privileges to read tokens: %v %s", resp, body)
			Error.Fatalf("Insufficient privileges to read tokens")
		}
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("Tokens: %s", string(body))

	var apikey_list map[string]interface{}
	err = json.Unmarshal(body, &apikey_list)
	if err != nil {
		Trace.Printf("ERR [Unmarshal] %s", err)
		panic(err)
	}

	value, err := jsonpath.Get("$.api_keys", apikey_list)
	if err != nil {
		Trace.Printf("ERR [jsonpath] %s", err)
		panic(err)
	}

	Trace.Printf("Obj: %v", value)

	t := table.NewWriter()
	t.SuppressTrailingSpaces()
	t.SetOutputMirror(os.Stdout)
	rowConfigAutoMerge := table.RowConfig{AutoMerge: true}
	t.AppendHeader(table.Row{"APIKey", "APIKey", "APIKey", "Role", "Indicies", "Indicies", "Indicies", "Indicies"}, rowConfigAutoMerge)
	t.AppendHeader(table.Row{"ID", "Name", "User", "Role", "Index", "Cluster", "Names", "Privileges"})
	t.SetColumnConfigs([]table.ColumnConfig{
		{Number: 1, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 25, AutoMerge: true},
		{Number: 2, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 20, AutoMerge: true},
		{Number: 3, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 20, AutoMerge: false},
		{Number: 4, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 25, AutoMerge: true},
		{Number: 5, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 10},
		{Number: 6, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 25},
		{Number: 7, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 25},
		{Number: 8, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 25},
	})
	t.SetCaption("APIKey List\n")
	t.Style().Options.SeparateRows = true
	t.SetAllowedRowLength(150)
	t.SortBy([]table.SortBy{
		{Number: 3, Mode: table.Asc},
		{Number: 2, Mode: table.Asc},
		{Number: 4, Mode: table.Asc},
		{Number: 5, Mode: table.Asc},
	})

	for _, token := range value.([]interface{}) {
		tn := token.(map[string]interface{})

		// skip if invalidated
		invalid, ok := tn["invalidated"]
		if ok {
			if invalid.(bool) {
				continue
			}
		}

		sec, dec := math.Modf(tn["creation"].(float64) / 1000)
		creation := time.Unix(int64(sec), int64(dec))

		var expiration time.Time
		_, ok = tn["expiration"]
		if ok {
			sec, dec = math.Modf(tn["expiration"].(float64) / 1000)
			expiration = time.Unix(int64(sec), int64(dec))
		} else {
			expiration = time.Unix(int64(0), int64(0))
		}
		// role_descriptors
		_, ok = tn["role_descriptors"]
		if !ok {
			t.AppendRow(table.Row{
				tn["id"], fmt.Sprintf("%s c: %s e: %s", tn["name"], creation, expiration), tn["username"], "None", "", "", "", ""},
				rowConfigAutoMerge)
			t.AppendSeparator()
		} else {
			for role, rd := range tn["role_descriptors"].(map[string]interface{}) {

				role_details := rd.(map[string]interface{})
				if len(role_details["indices"].([]interface{})) > 0 {
					indicies := role_details["indices"].([]interface{})
					for inum, idx := range indicies {
						i := idx.(map[string]interface{})
						privs := goterators.Map(i["privileges"].([]interface{}), func(item interface{}) string {
							return item.(string)
						})
						names := goterators.Map(i["names"].([]interface{}), func(item interface{}) string {
							return item.(string)
						})

						t.AppendRow(table.Row{
							tn["id"], fmt.Sprintf("%s c: %s e: %s", tn["name"], creation, expiration), tn["username"], role, inum, fmt.Sprintf("C: %v", role_details["cluster"].([]interface{})), fmt.Sprintf("N: %v", strings.Join(names, ", ")), fmt.Sprintf("P: %s", strings.Join(privs, ", "))},
							rowConfigAutoMerge)
						t.AppendSeparator()

					}

				}
			}
		}

		t.AppendSeparator()
	}
	setOutputAndRender(t, config)

	return nil
}

func APIKeyCreate(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)

	args := cCtx.Args()

	// must be two arguments or none
	var username string
	var password string
	if args.Present() {
		if args.Len() != 2 {
			Error.Fatalf("Must provide username and password arguments of the user for the APIKey to be created")
		} else {
			username = args.First()
			password = args.Get(1)
			if len(password) < 3 {
				Error.Fatalf("Supplied password must be a reasonable complexity (12 chars, uper, lower, digit, special chars): %s", password)
			}
		}
	} else {
		username = config.User
		password = config.Password
	}

	// user name is lowercase and starts with a letter
	is_alphanumeric := regexp.MustCompile(`^[a-z][a-z0-9]*$`).MatchString(username)
	if !is_alphanumeric {
		Error.Fatalf("Supplied username must be alpha-numeric starting with a letter: %s", username)
	}
	// build the role creation query
	var role_creation_body = RolePermissionsDefault
	if rts, err := loadRoleTemplateFile(config.RoleTemplateFile); err == nil {
		role_creation_body = rts
	} else {
		Trace.Printf("Could not load role template: %s / %v", config.RoleTemplateFile, err)
	}

	// build the user token creation query
	var reqdoc string
	var uri string
	if config.User == username {
		// user is doing own
		uri = "/_security/api_key?pretty"
		reqdoc = fmt.Sprintf(`
		{
			"name": "%s-elktail-read-token",
			"expiration": "%s",
			"role_descriptors": {
				"role-elktail-read":  %s
			}
		}
		`, username, config.APIKeyExpiryTime, role_creation_body)
		Trace.Printf("User is creating own token: %s / %s", username, reqdoc)
	} else {
		// grant on behalf of
		uri = "/_security/api_key/grant?pretty"
		reqdoc = fmt.Sprintf(`
		{
			"grant_type": "password",
			"username" : "%s",
			"password" : "%s",
			"api_key" : {
				"name": "%s-elktail-read-token",
				"expiration": "%s",
				"role_descriptors": {
					"role-elktail-read":  %s
				}
			}
		}
		`, username, password, username, config.APIKeyExpiryTime, role_creation_body)
		Trace.Printf("User %s is creating token on belhaf: %s / %s", config.User, username, reqdoc)
	}
	// no apikey for administrative calls
	config.SearchTarget.APIKey = ""

	client := request.BuildClient(config)

	// request for creating a token
	req := request.BuildRequest(config, uri, reqdoc, "POST")
	Trace.Printf("Req: %v", req)

	resp, err := client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}
	if resp.StatusCode == 403 {
		Trace.Printf("Insufficient privileges to create APIKeys: %v %s", resp, body)
		Error.Fatalf("Insufficient privileges to create APIKeys")
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("APIKey: %s", string(body))

	// declaring a struct
	var token map[string]interface{}
	err = json.Unmarshal([]byte(body), &token)
	if err != nil {
		Error.Fatalf("Response not a JSON object: %s/%s", string(body), err)
	}
	fmt.Printf("ELKTAIL_APIKEY=%s\n", token["encoded"])

	return nil
}

func APIKeyDelete(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)
	args := cCtx.Args()

	// can be one or more arguments
	if !args.Present() || args.Len() != 1 {
		Error.Fatalf("Must provide a APIKey ID argument for the APIKey to be deleted")
	}
	apikeyid := args.First()

	client := request.BuildClient(config)

	// apikey is lowercase and starts with a letter
	is_alphanumeric := regexp.MustCompile(`^[a-zA-Z0-9\_\-]*$`).MatchString(apikeyid)
	if !is_alphanumeric {
		Error.Fatalf("Supplied apikeyid must be alpha-numeric starting with a letter: %s", apikeyid)
	}

	// request for checking existence of the apikeyid
	req := request.BuildRequest(config, fmt.Sprintf("/_security/api_key?id=%s&active_only=true&pretty", apikeyid), "", "GET")
	Trace.Printf("Req: %v", req)
	resp, err := client.Do(req)
	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	if resp.StatusCode != 200 {
		// active_only or super rights may not be available in this version of ElasticSearch
		req = request.BuildRequest(config, fmt.Sprintf("/_security/api_key?id=%s&owner=true&active_only=true&pretty", apikeyid), "", "GET")
		Trace.Printf("Req: %v", req)

		resp, err = client.Do(req)

		if err != nil {
			Error.Fatalf("http.Request failed: %s", err)
		}
		if resp.StatusCode == 403 {
			body, _ := io.ReadAll(resp.Body)
			Trace.Printf("Insufficient privileges to read apikeys: %v %s", resp, body)
			Error.Fatalf("Insufficient privileges to read apikeys")
		}
	}

	if resp.StatusCode == 400 {
		// active_only may not be available in this version of ElasticSearch - try without
		req = request.BuildRequest(config, fmt.Sprintf("/_security/api_key?id=%s&pretty", apikeyid), "", "GET")
		Trace.Printf("Req: %v", req)
		resp, err = client.Do(req)
		if err != nil {
			Error.Fatalf("http.Request failed: %s", err)
		}
	}

	Trace.Printf("apikey check req status: %v / %v", resp.StatusCode, resp.Status)
	if resp.StatusCode != 200 {
		Error.Fatalf("APIKey does not exist: %s.", apikeyid)
	}
	if !config.Confirm {
		Error.Fatalf("Do you really want to delete APIKey: %s. Use --confirm to force delete", apikeyid)
	}

	// no apikey for administrative calls
	config.SearchTarget.APIKey = ""

	// request for deleting a apikey
	apikey_delete_body := fmt.Sprintf(`
	{
		"ids" : [ "%s" ]
	  }
	`, apikeyid)
	req = request.BuildRequest(config, "/_security/api_key?&pretty", apikey_delete_body, "DELETE")
	Trace.Printf("Req: %v", req)

	resp, err = client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	if resp.StatusCode != 200 {
		apikey_delete_body := fmt.Sprintf(`
		{
			"ids" : [ "%s" ],
			"owner": true
		  }
		`, apikeyid)
		req = request.BuildRequest(config, "/_security/api_key?&pretty", apikey_delete_body, "DELETE")
		Trace.Printf("Req: %v", req)

		resp, err = client.Do(req)

		if err != nil {
			Error.Fatalf("http.Request failed: %s", err)
		}
		body, err = io.ReadAll(resp.Body)
		if err != nil {
			Error.Fatalf("http.Request body read failed: %s", err)
		}

		Trace.Printf("Resp: %v", resp)
		Trace.Printf("apikey delete request: %s", string(body))
		if resp.StatusCode == 403 {
			Trace.Printf("Insufficient privileges to delete APIKeys: %v %s", resp, body)
			Error.Fatalf("Insufficient privileges to delete APIKeys")
		}
	}

	// declaring a struct
	var res map[string]interface{}
	err = json.Unmarshal([]byte(body), &res)
	if err != nil {
		Error.Fatalf("Response not a JSON object: %s/%s", string(body), err)
	}
	msg := "No"
	if resp.StatusCode == 200 {
		apikeyres, ok := res["invalidated_api_keys"]
		if ok {
			apikeys := goterators.Map(apikeyres.([]interface{}), func(item interface{}) string {
				return item.(string)
			})
			if slices.Contains(apikeys, apikeyid) {
				msg = "Yes"
			}
		}
	}
	fmt.Printf("APIKey %s deleted: %s\n", apikeyid, msg)
	return nil
}

func APIKeyConfig(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, true)
	args := cCtx.Args()
	config.ConfigFile = ""

	// can be one or more arguments
	config.SearchTarget.APIKey = ""
	if args.Present() {
		if args.Len() != 1 {
			Error.Fatalf("Must provide a APIKey ID argument for the APIKey to be deleted")
		}
		config.SearchTarget.APIKey = args.First()
	}
	// User and Password are personal so remove them
	config.User = ""
	config.Password = ""
	if len(config.QueryDefinition.Terms) == 0 {
		config.QueryDefinition.Terms = []string{"input.type:", "log"}
	}

	if config.SearchTarget.CaCert != "" {
		if len(config.SearchTarget.CaCert) > 1024 {
			Info.Printf("CaCert filename is too long (%s), attempting to use inline cert", config.SearchTarget.CaCert)
		} else {

			if _, err := os.Stat(config.SearchTarget.CaCert); errors.Is(err, os.ErrNotExist) {
				Info.Printf("CaCert file does not exist (%s), attempting to use inline cert", config.SearchTarget.CaCert)
			} else {
				caCert, err := os.ReadFile(config.SearchTarget.CaCert)
				if err != nil {
					Error.Fatalf("Bad ca certificate file: %s", err)
				} else {
					config.SearchTarget.CaCert = string(caCert)
				}
			}
		}
	}

	// Only use CaCert and API Key if present - Key and Cert are personal!
	config.SearchTarget.Key = ""
	config.SearchTarget.Cert = ""

	confData, err := yaml.Marshal(config)
	if err != nil {
		Error.Printf("Failed to marshall configuration to json: %s.\n", err)
		return nil
	}
	fmt.Printf("---\n%s\n", string(confData))
	if config.Confirm {
		fmt.Printf("\nComandLine: %s\n", config.PrintCmdLine())
	}
	return nil
}
