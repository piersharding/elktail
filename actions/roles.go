/* Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
package actions

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/ledongthuc/goterators"
	"github.com/urfave/cli/v2"
	"gitlab.com/piersharding/elktail/configuration"
	request "gitlab.com/piersharding/elktail/req"
)

func RoleList(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)

	client := request.BuildClient(config)

	// curl --cert config/ca/kibana-tls-signed.crt --key config/ca/kibana-tls.key --cacert config/ca/ca-bundle.pem \
	// -X GET "https://elastic:elastic_yolo@elasticsearch.local.net:9200/_security/role?pretty" -H 'Content-Type: application/json'

	req := request.BuildRequest(config, "/_security/role?pretty", "", "GET")
	Trace.Printf("Req: %v", req)

	resp, err := client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("Roles: %s", string(body))

	if resp.StatusCode == 401 || resp.StatusCode == 403 {
		Error.Printf("Unable to authenticate user for this role action: %s.  Check username:password supplied", resp.Status)
		_ = cli.ShowAppHelp(cCtx)
		os.Exit(1)
	}

	var role_list map[string]interface{}
	err = json.Unmarshal(body, &role_list)
	if err != nil {
		Trace.Printf("ERR [Unmarshal] %s", err)
		panic(err)
	}

	Trace.Printf("Obj: %v", role_list)

	t := table.NewWriter()
	t.SuppressTrailingSpaces()
	t.SetOutputMirror(os.Stdout)
	rowConfigAutoMerge := table.RowConfig{AutoMerge: true}
	t.AppendHeader(table.Row{"Role", "Indicies", "Indicies", "Indicies", "Indicies"}, rowConfigAutoMerge)
	t.AppendHeader(table.Row{"Role", "Index", "Cluster", "Names", "Privileges"})
	t.SetColumnConfigs([]table.ColumnConfig{
		{Number: 1, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 20, AutoMerge: true},
		{Number: 2, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 10},
		{Number: 3, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 50},
		{Number: 4, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 25},
		{Number: 5, Align: text.AlignLeft, VAlign: text.VAlignTop, WidthMax: 25},
	})
	t.Style().Options.SeparateRows = true
	t.SetCaption("Role List\n")
	t.SetAllowedRowLength(150)
	t.SortBy([]table.SortBy{
		{Number: 1, Mode: table.Asc},
		{Number: 2, Mode: table.Asc},
	})

	// can be one or more arguments
	rn := ""
	args := cCtx.Args()
	if args.Present() || args.Len() >= 1 {
		// Error.Fatalf("Must provide a rolename argument for the role to be created")
		rn = args.First()
	}

	for rolename, role := range role_list {

		// filter roles
		if rn != "" && !strings.Contains(strings.ToLower(rolename), strings.ToLower(rn)) {
			continue
		}
		role_details := role.(map[string]interface{})
		if len(role_details["indices"].([]interface{})) > 0 {
			indicies := role_details["indices"].([]interface{})
			for nr, idx := range indicies {
				i := idx.(map[string]interface{})
				privs := goterators.Map(i["privileges"].([]interface{}), func(item interface{}) string {
					return item.(string)
				})

				names := goterators.Map(i["names"].([]interface{}), func(item interface{}) string {
					return item.(string)
				})

				t.AppendRow(table.Row{
					fmt.Sprintf("%s", rolename), fmt.Sprintf("%d", nr), fmt.Sprintf("C: %v", role_details["cluster"].([]interface{})), fmt.Sprintf("N: %v", strings.Join(names, ", ")), fmt.Sprintf("P: %s", strings.Join(privs, ", "))},
					rowConfigAutoMerge)
				t.AppendSeparator()
			}
			t.AppendSeparator()

		}

		t.AppendSeparator()
	}
	setOutputAndRender(t, config)

	return nil
}

var RoleNameDefault = "elktail-viewer"

// {
// 	"cluster": ["monitor"],
// 	"indices": [
// 	  {
// 		"names": ["*"],
// 		"privileges": ["read","view_index_metadata", "monitor"]
// 	  }
// 	]
//   }

// build the role creation query
// "monitor" is required to cat indicies
// "manage_own_api_key" is required to create own API Token
var RolePermissionsDefault = `
	{
		"cluster" : ["monitor", "manage_own_api_key"],
		"indices" : [
		  {
			"names": ["*"],
			"privileges": ["monitor"]
		  },
		  {
			"names" : [
			  "/~(([.]|ilm-history-).*)/"
			],
			"privileges" : [
			  "read",
			  "view_index_metadata"
			],
			"allow_restricted_indices" : false
		  },
		  {
			"names" : [
			  "filebeat-*/",
			  ".ds-filebeat-*/"
			],
			"privileges" : [
			  "read",
			  "view_index_metadata"
			],
			"allow_restricted_indices" : false
		  },
		  {
			"names" : [
			  ".siem-signals*",
			  ".lists-*",
			  ".items-*"
			],
			"privileges" : [
			  "read",
			  "view_index_metadata"
			],
			"allow_restricted_indices" : false
		  },
		  {
			"names" : [
			  ".alerts*",
			  ".preview.alerts*"
			],
			"privileges" : [
			  "read",
			  "view_index_metadata"
			],
			"allow_restricted_indices" : false
		  },
		  {
			"names" : [
			  "profiling-*",
			  ".profiling-*"
			],
			"privileges" : [
			  "read",
			  "view_index_metadata"
			],
			"allow_restricted_indices" : false
		  }
		],
		"applications" : [
			{
				"application" : "kibana-.kibana",
				"privileges" : [
				"read"
				],
				"resources" : [
				"*"
				]
			}
		],
		"run_as" : [ ],
		"metadata" : {
			"created_by" : "elkuser"
		}
	}
   `

func RoleCreate(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)
	args := cCtx.Args()
	rolename := RoleNameDefault

	// can be one or more arguments
	if args.Present() || args.Len() >= 1 {
		// Error.Fatalf("Must provide a rolename argument for the role to be created")
		rolename = args.First()
	}

	client := request.BuildClient(config)

	// user name is lowercase and starts with a letter
	is_alphanumeric := regexp.MustCompile(`^[a-z][a-z0-9\-]*$`).MatchString(rolename)
	if !is_alphanumeric {
		Error.Fatalf("Supplied rolename must be alpha-numeric starting with a letter: %s", rolename)
	}

	// request for checking existence of role
	req := request.BuildRequest(config, fmt.Sprintf("/_security/role/%s?pretty", rolename), "", "GET")
	Trace.Printf("Req: %v", req)
	resp, err := client.Do(req)
	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	Trace.Printf("Role check req status: %v / %v", resp.StatusCode, resp.Status)
	updating := false
	if resp.StatusCode != 404 {
		if !config.Confirm {
			Error.Fatalf("Role already exists: %s. Use --confirm to force update", rolename)
		} else {
			updating = true
		}
	}

	// build the role creation query
	var role_creation_body = RolePermissionsDefault
	if rts, err := loadRoleTemplateFile(config.RoleTemplateFile); err == nil {
		role_creation_body = rts
	} else {
		Trace.Printf("Could not load role template: %s / %v", config.RoleTemplateFile, err)
	}

	// no apikey for administrative calls
	config.SearchTarget.APIKey = ""

	method := "POST"
	if updating {
		method = "PUT"
	}
	// request for creating/updating a role
	req = request.BuildRequest(config, fmt.Sprintf("/_security/role/%s?pretty", rolename), role_creation_body, method)
	Trace.Printf("Req: %v", req)

	resp, err = client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("Role request: %s", string(body))

	// declaring a struct
	var res map[string]interface{}
	err = json.Unmarshal([]byte(body), &res)
	if err != nil {
		Error.Fatalf("Response not a JSON object: %s/%s", string(body), err)
	}
	msg := "No"
	roleres, ok := res["role"]
	if ok {
		rolecreated := roleres.(map[string]interface{})
		created, ok := rolecreated["created"]
		if ok {
			if created.(bool) {
				msg = "Yes"
			}
		}
	}
	if updating && resp.StatusCode == 200 {
		fmt.Printf("Role %s updated: Yes\n", rolename)
	} else {
		fmt.Printf("Role %s created: %s\n", rolename, msg)
	}
	return nil
}

func RoleDelete(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)
	args := cCtx.Args()
	rolename := RoleNameDefault

	// can be one or more arguments
	if args.Present() || args.Len() >= 1 {
		// Error.Fatalf("Must provide a rolename argument for the role to be created")
		rolename = args.First()
	}

	client := request.BuildClient(config)

	// user name is lowercase and starts with a letter
	is_alphanumeric := regexp.MustCompile(`^[a-z][a-z0-9\-]*$`).MatchString(rolename)
	if !is_alphanumeric {
		Error.Fatalf("Supplied rolename must be alpha-numeric starting with a letter: %s", rolename)
	}

	// request for checking existence of role
	req := request.BuildRequest(config, fmt.Sprintf("/_security/role/%s?pretty", rolename), "", "GET")
	Trace.Printf("Req: %v", req)
	resp, err := client.Do(req)
	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	Trace.Printf("Role check req status: %v / %v", resp.StatusCode, resp.Status)
	if resp.StatusCode != 200 {
		Error.Fatalf("Role does not exist: %s.", rolename)
	}
	if !config.Confirm {
		Error.Fatalf("Do you really want to delete: %s. Use --confirm to force delete", rolename)
	}

	// no apikey for administrative calls
	config.SearchTarget.APIKey = ""

	// request for creating/updating a role
	req = request.BuildRequest(config, fmt.Sprintf("/_security/role/%s?pretty", rolename), "", "DELETE")
	Trace.Printf("Req: %v", req)

	resp, err = client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("Role request: %s", string(body))

	// declaring a struct
	var res map[string]interface{}
	err = json.Unmarshal([]byte(body), &res)
	if err != nil {
		Error.Fatalf("Response not a JSON object: %s/%s", string(body), err)
	}
	msg := "No"
	if resp.StatusCode == 200 {
		roleres, ok := res["found"]
		if ok {
			if roleres.(bool) {
				msg = "Yes"
			}
		}
	}
	fmt.Printf("Role %s deleted: %s\n", rolename, msg)
	return nil
}
