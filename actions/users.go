/* Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
package actions

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/urfave/cli/v2"
	"gitlab.com/piersharding/elktail/configuration"
	request "gitlab.com/piersharding/elktail/req"
)

func UserList(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)

	// get filter criteria iff provided
	un := ""
	args := cCtx.Args()
	if args.Present() || args.Len() >= 1 {
		un = args.First()
	}

	client := request.BuildClient(config)

	// curl --cert config/ca/kibana-tls-signed.crt --key config/ca/kibana-tls.key --cacert config/ca/ca-bundle.pem \
	// -X GET "https://elastic:elastic_yolo@elasticsearch.local.net:9200/_security/user?pretty" -H 'Content-Type: application/json'

	// request for creating a token
	req := request.BuildRequest(config, "/_security/user?pretty", "", "GET")
	Trace.Printf("Req: %v", req)

	resp, err := client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("Users: %s", string(body))
	if resp.StatusCode != 200 {
		Error.Fatalf("Insufficient privileges to list users")
	}

	var user_list map[string]interface{}
	err = json.Unmarshal(body, &user_list)
	if err != nil {
		Trace.Printf("ERR [Unmarshal] %s", err)
		panic(err)
	}
	Trace.Printf("Obj: %v", user_list)

	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"Username", "Full Name", "Email", "Roles"})
	t.SetColumnConfigs([]table.ColumnConfig{
		{Name: "Username", Align: text.AlignLeft, VAlign: text.VAlignTop},
		{Name: "Full Name", Align: text.AlignLeft, VAlign: text.VAlignTop},
		{Name: "Email", Align: text.AlignLeft, VAlign: text.VAlignTop},
		{Name: "Roles", Align: text.AlignLeft, VAlign: text.VAlignTop},
	})
	t.SetCaption("User List\n")
	t.Style().Options.SeparateRows = true
	t.SetAllowedRowLength(150)
	t.SortBy([]table.SortBy{
		{Name: "Username", Mode: table.Asc},
	})

	first := true
	for _, user := range user_list {
		// fmt.Println(user)
		u := user.(map[string]interface{})
		username := u["username"]
		if u["username"] == nil {
			username = ""
		}

		// filter users
		if un != "" && !strings.Contains(strings.ToLower(username.(string)), strings.ToLower(un)) {
			continue
		}

		full_name := u["full_name"]
		if u["full_name"] == nil {
			full_name = ""
		}
		email := u["email"]
		if u["email"] == nil {
			email = ""
		}
		enabled := "❌"
		if u["enabled"].(bool) {
			enabled = "✅"
		}
		var names []string
		for i, r := range u["roles"].([]interface{}) {
			prepend := ""
			if i%2 != 0 {
				prepend = "\n"
			}
			names = append(names, prepend+r.(string))
		}
		// fmt.Printf("id: %s name: %s email: %s roles: %s \n", fmt.Sprintf("[%s] %s", enabled, username), full_name, email, strings.Join(names, ", "))
		if !first {
			t.AppendSeparator()
		}
		first = false

		t.AppendRows([]table.Row{
			{fmt.Sprintf("[%s] %s", enabled, username), full_name, email, strings.Join(names, ", ")},
		})
	}
	t.AppendSeparator()
	setOutputAndRender(t, config)

	return nil
}

func UserCreate(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)
	args := cCtx.Args()

	// must be one argument
	if !args.Present() || !(args.Len() >= 1 && args.Len() <= 2) {
		Error.Fatalf("Must provide a username and optional password argument for the user to be created")
	}

	client := request.BuildClient(config)

	username := args.First()
	// user name is lowercase and starts with a letter
	is_alphanumeric := regexp.MustCompile(`^[a-z][a-z0-9]*$`).MatchString(username)
	if !is_alphanumeric {
		Error.Fatalf("Supplied username must be alpha-numeric starting with a letter: %s", username)
	}
	password := args.Get(1)

	// empty password
	if password == "" {
		password = genPassword(12)
	}
	is_alphanumeric = regexp.MustCompile(`^[a-zA-Z][a-zA-Z0-9\~\=\+\%\^\*\/\(\)\[\]\{\}\/\!\@\#\$\?\|]*$`).MatchString(password)
	if !is_alphanumeric {
		Error.Fatalf("Supplied password must be a reasonable complexity (12 chars, upper, lower, digit, special chars): %s", password)
	}

	jsonRoleNames, err := json.Marshal(config.RoleNames.Value())
	if err != nil {
		Error.Fatalf("Could not serialise rolenames: %s", config.RoleNames.Value())
	}
	Trace.Printf("User to create: %s/%s/%s", username, password, jsonRoleNames)

	// request for checking existence of role
	for _, role := range config.RoleNames.Value() {
		req := request.BuildRequest(config, fmt.Sprintf("/_security/role/%s?pretty", role), "", "GET")
		Trace.Printf("Req: %v", req)
		resp, err := client.Do(req)
		if err != nil {
			Error.Fatalf("http.Request failed: %s", err)
		}
		Trace.Printf("Role check req status: %v / %v", resp.StatusCode, resp.Status)
		if resp.StatusCode != 200 {
			Error.Fatalf("Supplied role does not exist: %s", role)
		}
	}

	// request for checking existence of role
	req := request.BuildRequest(config, fmt.Sprintf("/_security/user/%s?pretty", username), "", "GET")
	Trace.Printf("Req: %v", req)
	resp, err := client.Do(req)
	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	Trace.Printf("User check req status: %v / %v", resp.StatusCode, resp.Status)
	updating := false
	if resp.StatusCode != 404 {
		if !config.Confirm {
			Error.Fatalf("User already exists: %s. Use --confirm to force update", username)
		} else {
			updating = true
		}
	}

	method := "POST"
	if updating {
		method = "PUT"
	}

	// build the user creation query
	var user_creation_body = fmt.Sprintf(`
	{
		"password" : "%s",
		"roles" : %s,
		"full_name" : "%s",
		"email" : "%s",
		"metadata" : {
		  "created_by" : "elkuser"
		}
	  }
   `, password, jsonRoleNames, config.FullName, config.EmailAddress)

	// no apikey for administrative calls
	config.SearchTarget.APIKey = ""

	// request for creating a token
	req = request.BuildRequest(config, fmt.Sprintf("/_security/user/%s?pretty", username), user_creation_body, method)
	Trace.Printf("Req: %v", req)

	resp, err = client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("User request: %s", string(body))

	// declaring a struct
	var res map[string]interface{}
	err = json.Unmarshal([]byte(body), &res)
	if err != nil {
		Error.Fatalf("Response not a JSON object: %s/%s", string(body), err)
	}
	msg := "failed"
	if resp.StatusCode == 200 {
		userres, ok := res["created"]
		if ok {
			if userres.(bool) {
				msg = "created"
			}
		}
		userres, ok = res["found"]
		if ok {
			if userres.(bool) {
				msg = "created"
			}
		}
	}
	if updating && resp.StatusCode == 200 {
		msg = "updated"
	}

	fmt.Printf("User: %s %s \n", username, msg)
	if msg == "failed" {
		return nil
	}
	fmt.Printf("Password: %s\n", password)
	return nil
}

func UserChangePassword(cCtx *cli.Context) error {
	// https://www.elastic.co/guide/en/elasticsearch/reference/current/security-api-change-password.html
	config := configuration.Config
	setUp(cCtx, false)
	args := cCtx.Args()

	// must be one argument
	if !args.Present() || !(args.Len() >= 1 && args.Len() <= 2) {
		Error.Fatalf("Must provide a username and new-password arguments or just the new-password argument for the user password to be updated (old password with --user u:p)")
	}

	client := request.BuildClient(config)

	username := args.First()
	// if we only have the password arg
	if args.Len() == 1 {
		username = config.User
	}
	// user name is lowercase and starts with a letter
	is_alphanumeric := regexp.MustCompile(`^[a-z][a-z0-9]*$`).MatchString(username)
	if !is_alphanumeric {
		Error.Fatalf("Supplied username must be alpha-numeric starting with a letter: %s", username)
	}
	password := args.Get(1)
	// if we only have the password arg
	if args.Len() == 1 {
		password = args.First()
	}

	is_alphanumeric = regexp.MustCompile(`^[a-zA-Z][a-zA-Z0-9\~\=\+\%\^\*\/\(\)\[\]\{\}\/\!\@\#\$\?\|]*$`).MatchString(password)
	if !is_alphanumeric {
		Error.Fatalf("Supplied password must be a reasonable complexity (12 chars, upper, lower, digit, special chars): %s", password)
	}

	Trace.Printf("User password to update: %s/%s", username, password)

	// request for checking existence of role
	req := request.BuildRequest(config, fmt.Sprintf("/_security/user/%s?pretty", username), "", "GET")
	Trace.Printf("Req: %v", req)
	resp, err := client.Do(req)
	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	Trace.Printf("User check req status: %v / %v", resp.StatusCode, resp.Status)
	if resp.StatusCode == 404 {
		Error.Fatalf("User does not exist: %s.", username)
	}

	// build the user creation query
	var user_update_password_body = fmt.Sprintf(`
    {
        "password" : "%s"
    }
   `, password)

	// no apikey for administrative calls
	config.SearchTarget.APIKey = ""

	// request for updating password
	req = request.BuildRequest(config, fmt.Sprintf("/_security/user/%s/_password?pretty", username), user_update_password_body, "POST")
	Trace.Printf("Req: %v", req)

	resp, err = client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("User update password request: %s", string(body))

	// declaring a struct
	var res map[string]interface{}
	err = json.Unmarshal([]byte(body), &res)
	if err != nil {
		Error.Fatalf("Response not a JSON object: %s/%s", string(body), err)
	}
	msg := "failed"
	if resp.StatusCode == 200 {
		msg = "updated"
	} else {
		msg = "failed"
		err, ok := res["error"]
		if ok {
			t, ok := err.(map[string]interface{})["type"]
			if ok {
				msg = msg + ": " + t.(string)
			}
			r, ok := err.(map[string]interface{})["reason"]
			if ok {
				msg = msg + " - " + r.(string)
			}
		}
	}

	fmt.Printf("User: %s %s \n", username, msg)
	if strings.HasPrefix(msg, "failed") {
		os.Exit(1)
	}
	fmt.Printf("Password: %s\n", password)
	return nil
}

func UserDelete(cCtx *cli.Context) error {
	config := configuration.Config
	setUp(cCtx, false)
	args := cCtx.Args()

	// can be one or more arguments
	if !args.Present() || args.Len() != 1 {
		Error.Fatalf("Must provide a username argument for the user to be deleted")
	}
	username := args.First()

	client := request.BuildClient(config)

	// user name is lowercase and starts with a letter
	is_alphanumeric := regexp.MustCompile(`^[a-z][a-z0-9\-]*$`).MatchString(username)
	if !is_alphanumeric {
		Error.Fatalf("Supplied username must be alpha-numeric starting with a letter: %s", username)
	}

	// request for checking existence of user
	req := request.BuildRequest(config, fmt.Sprintf("/_security/user/%s?pretty", username), "", "GET")
	Trace.Printf("Req: %v", req)
	resp, err := client.Do(req)
	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	Trace.Printf("User check req status: %v / %v", resp.StatusCode, resp.Status)
	if resp.StatusCode != 200 {
		Error.Fatalf("User does not exist: %s.", username)
	}
	if !config.Confirm {
		Error.Fatalf("Do you really want to delete: %s. Use --confirm to force delete", username)
	}

	// no apikey for administrative calls
	config.SearchTarget.APIKey = ""

	// request for creating/updating a role
	req = request.BuildRequest(config, fmt.Sprintf("/_security/user/%s?pretty", username), "", "DELETE")
	Trace.Printf("Req: %v", req)

	resp, err = client.Do(req)

	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("User request: %s", string(body))

	// declaring a struct
	var res map[string]interface{}
	err = json.Unmarshal([]byte(body), &res)
	if err != nil {
		Error.Fatalf("Response not a JSON object: %s/%s", string(body), err)
	}
	msg := "No"
	if resp.StatusCode == 200 {
		userres, ok := res["found"]
		if ok {
			if userres.(bool) {
				msg = "Yes"
			}
		}
	}
	fmt.Printf("User %s deleted: %s\n", username, msg)
	return nil
}
