/*
 * Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/jedib0t/go-pretty/v6/list"
	"github.com/ledongthuc/goterators"
	"github.com/urfave/cli/v2"
	configuration "gitlab.com/piersharding/elktail/configuration"
	"gitlab.com/piersharding/elktail/logging"
	request "gitlab.com/piersharding/elktail/req"
	ssh "gitlab.com/piersharding/elktail/ssh"
	tail "gitlab.com/piersharding/elktail/tail"
	version "gitlab.com/piersharding/elktail/version"

	"github.com/manifoldco/promptui"
)

var (
	Trace *log.Logger
	Info  *log.Logger
	Error *log.Logger
)

func main() {

	config := new(configuration.Configuration)
	app := cli.NewApp()
	app.Name = "elktail"
	app.Usage = "utility for tailing Filebeat logs stored in ElasticSearch"
	app.HideHelp = true
	app.Version = version.VERSION
	app.ArgsUsage = "[query-string]\n   Options marked with (*) are saved between invocations of the command. Each time you specify an option marked with (*) previously stored settings are erased."
	config.DefineCoreFlags()
	app.Flags = config.Flags(false)
	sort.Sort(cli.FlagsByName(app.Flags))
	cli.AppHelpTemplate = fmt.Sprintf("\n%s\n\nFor extended help, go to https://gitlab.com/piersharding/elktail\n\n", cli.AppHelpTemplate)
	app.OnUsageError = func(c *cli.Context, err error, isSubcommand bool) error {
		fmt.Fprintf(c.App.Writer, "Incorrect invocation: %s\n", err.Error())
		_ = cli.ShowAppHelp(c)
		os.Exit(1)
		// will never reach this!
		return nil
	}

	app.Action = func(c *cli.Context) error {
		if c.IsSet("help") {
			_ = cli.ShowAppHelp(c)
			os.Exit(0)
		}
		if config.MoreVerbose || config.TraceRequests {
			logging.InitLogging(os.Stderr, os.Stderr, os.Stderr, true)
		} else if config.Verbose {
			logging.InitLogging(io.Discard, os.Stderr, os.Stderr, false)
		} else {
			logging.InitLogging(io.Discard, io.Discard, os.Stderr, false)
		}

		Trace = logging.Trace
		Info = logging.Info
		Error = logging.Error
		if config.MoreVerbose || config.TraceRequests {
			confJs, _ := json.MarshalIndent(config, "", "  ")
			Trace.Printf("Configuration BEFORE load (from commandline) is: %s", string(confJs))
		}

		loadedConfig, err := configuration.LoadDefault(config.ConfigFile)
		if err != nil {
			Info.Printf("Failed to find or open previous default configuration: %s\n", err)
		} else {
			Trace.Printf("Loaded previous config: %v\n", loadedConfig)
			loadedConfig.Copy(config, c)
			config.ConfigFile = loadedConfig.ConfigFile
			Info.Printf("Loaded previous config and connecting to host %s.\n", loadedConfig.SearchTarget.Url)

		}
		if config.MoreVerbose || config.TraceRequests {
			confJs, _ := json.MarshalIndent(config, "", "  ")
			Trace.Printf("Configuration AFTER load is: %s", string(confJs))
		}

		if config.User != "" {
			credentials := strings.Split(config.User, ":")
			config.User = credentials[0]
			if len(credentials) == 2 {
				config.Password = credentials[1]
			}
		}
		Trace.Printf("Configuration before we go in is: %#v", config)

		//reset TunnelUrl to nothing, we'll point to the tunnel if we actually manage to create it
		config.SearchTarget.TunnelUrl = ""
		if config.SSHTunnelParams != "" {
			//We need to start ssh tunnel and make el client connect to local port at localhost in order to pass
			//traffic through the tunnel
			elurl, err := url.Parse(config.SearchTarget.Url)
			if err != nil {
				Error.Fatalf("Failed to parse hostname/port from given URL: %s\n", config.SearchTarget.Url)
			}
			Trace.Printf("SSHTunnel remote host: %s\n", elurl.Host)

			tunnel := ssh.NewSSHTunnelFromHostStrings(config.SSHTunnelParams, config.SSHTunnelHop, elurl.Host)
			//Using the TunnelUrl configuration param, we will signify the client to connect to tunnel
			tport := "http"
			if config.SearchTarget.CaCert != "" {
				tport = "https"
			}
			config.SearchTarget.TunnelUrl = fmt.Sprintf("%s://localhost:%d", tport, tunnel.Local.Port)

			Info.Printf("Starting SSH tunnel %d:%s@%s:%d to %s:%d", tunnel.Local.Port, tunnel.Config.User,
				tunnel.Server.Host, tunnel.Server.Port, tunnel.Remote.Host, tunnel.Remote.Port)
			go tunnel.Start()
			tunnel_wait := config.TunnelWait
			if config.SSHTunnelHop != "" && tunnel_wait < 3 {
				tunnel_wait = 3
			}
			Trace.Printf("Sleeping for %d second(s) until tunnel is established...", tunnel_wait)
			time.Sleep(time.Duration(tunnel_wait) * time.Second)
		}

		args := c.Args()

		if args.Present() {
			config.QueryDefinition.Terms = []string{args.First()}
			config.QueryDefinition.Terms = append(config.QueryDefinition.Terms, args.Tail()...)
		}

		// output Commandline that would have been called
		Info.Printf("Commandline: %s", config.PrintCmdLine())

		// setup configured tail
		tail := tail.NewTail(config, c)

		// output the field mappings and exit
		if config.PrintFields {
			printFields(tail, config, args)
			os.Exit(0)
		}

		//If we don't exit here we can save the defaults
		//Only save if explicitly asked to
		if config.SaveQuery {
			Trace.Printf("Saving query terms. Total terms: %d\n", len(config.QueryDefinition.Terms))

			// allow the user to edit the target config file
			validate_configfile := func(input string) error {
				if !strings.HasSuffix(input, ".yaml") && !strings.HasSuffix(input, ".json") {
					return errors.New("ConfigFile must have a .json or .yaml suffix")
				}
				return nil
			}

			prompt := promptui.Prompt{
				Label:    "ConfigFile",
				Validate: validate_configfile,
				Default:  config.ConfigFile,
			}

			config.ConfigFile, err = prompt.Run()
			if err != nil {
				Error.Fatalf("SaveQuery ConfigFile prompt failed %v\n", err)
				// return nil
			}

			Trace.Printf("The ConfigFile for save is %s\n", config.ConfigFile)
			config.SaveDefault()
		}

		// now get on with tailing ELK!
		for {
			tail.Start(!config.IsListOnly())
			if config.IsListOnly() {
				return nil
			}
			Info.Printf("Trying to setup the tail again")
		}
	}

	err := app.Run(os.Args)
	if err != nil {
		Error.Fatalf("app.Run aborted: %v", err)
	}

}

// Must is a helper function to avoid boilerplate error handling for regex matches
// this way they may be used in single value context
func Must(result bool, err error) bool {
	if err != nil {
		Error.Panic(err)
	}
	return result
}

func printFields(tail *tail.Tail, config *configuration.Configuration, args cli.Args) {
	indicies := tail.Indicies()
	idx := indicies[0]
	client := request.BuildClient(config)

	// make sure user/pass is not set for this
	config.User = ""
	// request for checking existence of the tokenid
	req := request.BuildRequest(config, fmt.Sprintf("/%s/_mapping?pretty", idx), "", "GET")
	// make sure that user/pass has not been previously set
	req.Header.Add("Authorization", "ApiKey "+config.SearchTarget.APIKey)
	Trace.Printf("Req: %v", req)
	resp, err := client.Do(req)
	if err != nil {
		Error.Fatalf("http.Request failed: %s", err)
	}
	Trace.Printf("Get mapping req status: %v / %v", resp.StatusCode, resp.Status)
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	if resp.StatusCode != 200 {
		Error.Fatalf("Mappings do not exist: %s.", idx)
	}

	// declaring a struct
	var res map[string]interface{}
	err = json.Unmarshal([]byte(body), &res)
	if err != nil {
		Error.Fatalf("Response not a JSON object: %s/%s", string(body), err)
	}

	// fields.
	l := list.NewWriter()
	l.Reset()
	idx_def := res[idx].(map[string]interface{})
	mappings := idx_def["mappings"].(map[string]interface{})
	dynamic_templates := mappings["dynamic_templates"].([]interface{})
	properties := mappings["properties"].(map[string]interface{})

	var fields = []string{}
	// dynamic fields
	for _, tpl := range dynamic_templates {
		for k, v := range tpl.(map[string]interface{}) {
			t := "unknown"
			m, ok := v.(map[string]interface{})["mapping"]
			if ok {
				m, ok := m.(map[string]interface{})["type"]
				if ok {
					t = m.(string)
				}
			}
			val, ok := v.(map[string]interface{})["path_match"]
			dfld := k + "[" + string(t[0]) + "]"
			if ok {
				dfld = val.(string) + "[" + string(t[0]) + "]"
			}
			fields = append(fields, dfld)
		}
	}

	Trace.Printf("fields: %v\n", fields)
	l.AppendItem("Field Properties of Indicies")
	l.Indent()
	l.AppendItem("Dynamic Fields")
	l.Indent()
	sort.Strings(fields)
	for _, k := range fields {
		l.AppendItem(k)
	}
	l.UnIndent()

	// static properties
	fields = []string{}
	for tplk, tpl := range properties {
		p := getProperties(tplk, tpl)
		fields = append(fields, p...)
	}
	l.AppendItem("Property Fields")
	l.Indent()
	sort.Strings(fields)
	term := ""
	if args.Present() || args.Len() >= 1 {
		term = args.First()
	}
	rterm, _ := regexp.Compile(strings.ToLower(term))
	for _, k := range fields {
		// filter properties
		if term != "" && !rterm.MatchString(strings.ToLower(k)) {
			continue
		}
		l.AppendItem(k)
	}
	setOutputStyle(l, config)
	fmt.Printf("\n%s\n", l.Render())
}

func getProperties(name string, prop interface{}) []string {

	var properties = []string{}
	Trace.Printf("Incoming prop: %v", prop)

	el := prop.(map[string]interface{})
	vel, ok := el["properties"]
	if ok {

		for tplk, tpl := range vel.(map[string]interface{}) {

			props := getProperties(tplk, tpl)
			props = goterators.Map(props, func(item string) string {
				return name + "." + item
			})
			properties = append(properties, props...)
		}

	} else {
		t := "unknown"
		Trace.Printf("before string %v", el)
		m, ok := el["type"]
		if ok {

			Trace.Printf("type of %v", m)

			switch typ := m.(type) {
			case string:
				t = m.(string)
			default:
				Trace.Printf("Got: %v", typ)
				mt, ok := m.(map[string]interface{})["type"]
				if ok {
					t = mt.(string)
				}
			}

		}
		properties = append(properties, name+"["+t+"]")
	}

	return properties
}

func setOutputStyle(l list.Writer, config *configuration.Configuration) {
	if config.OutputStyle == "bright" {
		l.SetStyle(list.StyleConnectedBold)
	} else if config.OutputStyle == "default" {
		l.SetStyle(list.StyleConnectedRounded)
	} else {
		l.SetStyle(list.StyleConnectedLight)
	}
}
