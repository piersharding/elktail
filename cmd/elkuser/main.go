/*
 * Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"sort"

	_ "github.com/PaesslerAG/jsonpath"
	_ "github.com/ledongthuc/goterators"

	"github.com/urfave/cli/v2"
	actions "gitlab.com/piersharding/elktail/actions"
	configuration "gitlab.com/piersharding/elktail/configuration"
	"gitlab.com/piersharding/elktail/logging"
	version "gitlab.com/piersharding/elktail/version"
)

var (
	Trace *log.Logger
	Info  *log.Logger
	Error *log.Logger
)

func main() {

	config := configuration.Config
	app := cli.NewApp()
	app.Name = "elkuser"
	app.Usage = "utility for creating user tokens for elktail users"
	app.HideHelp = true
	app.Version = version.VERSION
	app.ArgsUsage = "<sub-command>\n   Options marked with (*) are saved between invocations of the command. Each time you specify an option marked with (*) previously stored settings are erased."
	config.DefineCoreFlags()
	app.Flags = config.Flags(true)

	app.Commands = actions.Commands(config)
	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))

	cli.AppHelpTemplate = fmt.Sprintf("\n%s\n\nFor extended help, go to https://gitlab.com/piersharding/elktail\n\n", cli.AppHelpTemplate)
	app.OnUsageError = func(c *cli.Context, err error, isSubcommand bool) error {
		fmt.Fprintf(c.App.Writer, "Incorrect invocation: %s\n", err.Error())
		_ = cli.ShowAppHelp(c)
		os.Exit(1)
		// will never reach this!
		return nil
	}
	app.Action = actions.Main

	// setup logging
	if config.MoreVerbose || config.TraceRequests {
		logging.InitLogging(os.Stderr, os.Stderr, os.Stderr, true)
	} else if config.Verbose {
		logging.InitLogging(io.Discard, os.Stderr, os.Stderr, false)
	} else {
		logging.InitLogging(io.Discard, io.Discard, os.Stderr, false)
	}
	Trace = logging.Trace
	Info = logging.Info
	Error = logging.Error

	err := app.Run(os.Args)
	if err != nil {
		Error.Fatalf("app.Run aborted: %v", err)
	}

}

// Must is a helper function to avoid boilerplate error handling for regex matches
// this way they may be used in single value context
func Must(result bool, err error) bool {
	if err != nil {
		Error.Panic(err)
	}
	return result
}
