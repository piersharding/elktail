/* Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

package configuration

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/urfave/cli/v2"
	"gitlab.com/piersharding/elktail/logging"
	"sigs.k8s.io/yaml"
)

var (
	Trace *log.Logger
	Info  *log.Logger
	Error *log.Logger
)

type SearchTarget struct {
	Url          string
	TunnelUrl    string `json:"-"`
	IndexPattern string
	CaCert       string
	Cert         string
	Key          string
	Insecure     bool
	APIKey       string
	// ExtraHeaders []string
}

type QueryDefinition struct {
	Terms           []string
	Format          string
	TimestampField  string
	AfterDateTime   string `json:"-"`
	BeforeDateTime  string `json:"-"`
	ContextSpanTime string `json:"-"`
}

type Configuration struct {
	SearchTarget     SearchTarget
	QueryDefinition  QueryDefinition
	InitialEntries   int
	TunnelWait       int
	Follow           bool `json:"-"`
	Raw              bool `json:"-"`
	LineNo           bool `json:"-"`
	Descending       bool `json:"-"`
	Count            bool `json:"-"`
	AddId            bool `json:"-"`
	PrettyPrint      bool `json:"-"`
	PrintTerms       bool `json:"-"`
	PrintFields      bool `json:"-"`
	ConfigFile       string
	User             string
	Password         string
	Verbose          bool `json:"-"`
	MoreVerbose      bool `json:"-"`
	TraceRequests    bool `json:"-"`
	SSHTunnelParams  string
	SSHTunnelHop     string
	SocksProxy       string
	SaveQuery        bool            `json:"-"`
	Delimiter        bool            `json:"-"`
	Detail           bool            `json:"-"`
	Output           string          `json:"-"`
	Confirm          bool            `json:"-"`
	OutputStyle      string          `json:"-"`
	RoleNames        cli.StringSlice `json:"-"`
	FullName         string          `json:"-"`
	EmailAddress     string          `json:"-"`
	RoleTemplateFile string          `json:"-"`
	APIKeyExpiryTime string          `json:"-"`
	ServerVersion    int
}

var Config = new(Configuration)

var confDir = ".elktail"
var defaultConfFile = "default.json"

// Find the current users home dir
func UserHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	}
	return os.Getenv("HOME")
}

func (c *Configuration) Setup() {
	Trace = logging.Trace
	Info = logging.Info
	Error = logging.Error

}

func LoadDefault(confFile string) (conf *Configuration, err error) {
	conf.Setup()
	confDirPath := filepath.Dir(confFile)
	if _, err := os.Stat(confDirPath); os.IsNotExist(err) {
		//conf directory doesn't exist, let's create it
		err := os.Mkdir(confDirPath, 0700)
		if err != nil {
			return nil, err
		}
	}

	selectedConfFile := getConfigFileInteractive(confFile)
	if selectedConfFile == "" {
		return nil, fmt.Errorf("Invalid config file: %s", confFile)
	}

	var config *Configuration
	confBytes, err := os.ReadFile(selectedConfFile)
	if err != nil {
		return nil, err
	}
	if strings.HasSuffix(selectedConfFile, ".yaml") {
		err = yaml.Unmarshal(confBytes, &config)
	} else {
		err = json.Unmarshal(confBytes, &config)
	}
	if err != nil {
		return nil, err
	}
	return config, nil
}

func (c *Configuration) Copy(dest *Configuration, ctxt *cli.Context) *Configuration {

	// dest := new(Configuration)
	Trace = logging.Trace
	Info = logging.Info
	Error = logging.Error

	// c.CopyConfigRelevantSettingsTo(dest, ctxt)

	if !ctxt.IsSet("U") {
		dest.SearchTarget.Url = c.SearchTarget.Url

	}
	// if dest.SearchTarget.Url == "" {
	// 	dest.SearchTarget.Url = c.SearchTarget.Url
	// }
	if !ctxt.IsSet("i") {
		dest.SearchTarget.IndexPattern = c.SearchTarget.IndexPattern
	}
	if dest.SearchTarget.TunnelUrl == "" {
		dest.SearchTarget.TunnelUrl = c.SearchTarget.TunnelUrl
	}
	// dest.SearchTarget.ExtraHeaders = make([]string, len(c.SearchTarget.ExtraHeaders))
	// copy(dest.SearchTarget.ExtraHeaders, c.SearchTarget.ExtraHeaders)

	if !ctxt.IsSet("cacert") {
		dest.SearchTarget.CaCert = c.SearchTarget.CaCert
	}
	if !ctxt.IsSet("cert") {
		dest.SearchTarget.Cert = c.SearchTarget.Cert
	}
	if !ctxt.IsSet("key") {
		dest.SearchTarget.Key = c.SearchTarget.Key
	}
	if c.SearchTarget.Insecure {
		dest.SearchTarget.Insecure = c.SearchTarget.Insecure
	}
	if !ctxt.IsSet("apikey") {
		dest.SearchTarget.APIKey = c.SearchTarget.APIKey
	}
	if !ctxt.IsSet("t") {
		dest.QueryDefinition.TimestampField = c.QueryDefinition.TimestampField
	}
	if !ctxt.IsSet("F") {
		dest.QueryDefinition.Format = c.QueryDefinition.Format
	}
	if !ctxt.IsSet("socks") {
		dest.SocksProxy = c.SocksProxy
	}
	if !ctxt.IsSet("ssh") {
		dest.SSHTunnelParams = c.SSHTunnelParams
	}
	if !ctxt.IsSet("ssh-hop") {
		dest.SSHTunnelHop = c.SSHTunnelHop
	}
	if dest.TunnelWait <= 1 && c.TunnelWait > 0 {
		dest.TunnelWait = c.TunnelWait
	}

	dest.QueryDefinition.Terms = append(dest.QueryDefinition.Terms, c.QueryDefinition.Terms...)
	Trace.Printf("Copy: dest.User: %s = c.User: %s", dest.User, c.User)
	if !ctxt.IsSet("u") {
		dest.User = c.User
		dest.Password = c.Password
	}

	Trace.Printf("Copy: dest.Output/Style: %s = c.Output: %s", dest.Output, c.Output)
	dest.OutputStyle = "light"
	if !ctxt.IsSet("output") {
		dest.Output = c.Output
	} else {
		style := strings.Split(c.Output, "+")
		c.Output = style[0]
		if len(style) == 2 {
			c.OutputStyle = style[1]
		}
	}

	if !ctxt.IsSet("a") {
		dest.QueryDefinition.AfterDateTime = c.QueryDefinition.AfterDateTime
	}
	if !ctxt.IsSet("b") {
		dest.QueryDefinition.BeforeDateTime = c.QueryDefinition.BeforeDateTime
	}
	if !ctxt.IsSet("C") {
		dest.QueryDefinition.ContextSpanTime = c.QueryDefinition.ContextSpanTime
	}
	if c.Descending {
		dest.Descending = c.Descending
	}
	if c.Follow {
		dest.Follow = c.Follow
	}
	if c.Raw {
		dest.Raw = c.Raw
	}
	if c.LineNo {
		dest.LineNo = c.LineNo
	}
	if c.Count {
		dest.Count = c.Count
	}
	if c.AddId {
		dest.AddId = c.AddId
	}
	if c.PrettyPrint {
		dest.PrettyPrint = c.PrettyPrint
	}
	if c.PrintTerms {
		dest.PrintTerms = c.PrintTerms
	}
	if c.PrintFields {
		dest.PrintFields = c.PrintFields
	}
	if !ctxt.IsSet("c") {
		dest.ConfigFile = c.ConfigFile
	}
	if !ctxt.IsSet("n") {
		dest.InitialEntries = c.InitialEntries
	}
	// only set it if it isn't set already
	if c.TunnelWait > 0 {
		dest.TunnelWait = c.TunnelWait
	}
	if !ctxt.IsSet("d") {
		dest.Delimiter = c.Delimiter
	}
	dest.Verbose = c.Verbose
	dest.MoreVerbose = c.MoreVerbose
	dest.TraceRequests = c.TraceRequests
	Info.Printf("CopyNonConfigRelevantSettingsTo: dest.User: %s = c.User: %s", dest.User, c.User)

	return dest
}

// When changing this array, make sure to also make appropriate changes in CopyConfigRelevantSettingsTo
var configRelevantFlags = []string{"url", "i", "cert", "user", "key", "apikey", "t", "u", "ssh", "ssh-hop"}

func (c *Configuration) SaveDefault() {
	c.Setup()
	usr, _ := user.Current()
	dir := usr.HomeDir
	if c.ConfigFile == "~" {
		// In case of "~", which won't be caught by the "else if"
		c.ConfigFile = dir
	} else if strings.HasPrefix(c.ConfigFile, "~/") {
		// Use strings.HasPrefix so we don't match paths like
		// "/something/~/something/"
		c.ConfigFile = filepath.Join(dir, c.ConfigFile[2:])
	}
	confDirPath := filepath.Dir(c.ConfigFile)
	if _, err := os.Stat(confDirPath); os.IsNotExist(err) {
		Info.Printf("Config file Dir: %s / %v \n", confDirPath, err)
		//conf directory doesn't exist, let's create it
		err := os.Mkdir(confDirPath, 0700)
		if err != nil {
			Error.Fatalf("Failed to create configuration directory %s, %s\n", confDirPath, err)
		}
	}
	var confData []byte
	var err error
	if strings.HasSuffix(c.ConfigFile, ".yaml") {
		confData, err = yaml.Marshal(c)
	} else {
		confData, err = json.MarshalIndent(c, "", "  ")
	}

	if err != nil {
		Error.Fatalf("Failed to marshall configuration to json: %s.\n", err)
	}
	err = os.WriteFile(c.ConfigFile, confData, 0700)
	if err != nil {
		Error.Fatalf("Failed to save configuration to file %s, %s\n", c.ConfigFile, err)
	}
	Trace.Printf("Saved config to: %s", c.ConfigFile)
}

var ConfFile string

func (config *Configuration) DefineCoreFlags() {
	cli.VersionFlag = &cli.BoolFlag{
		Name:     "print-version",
		Aliases:  []string{"V"},
		Usage:    "Print version",
		Category: "GLOBAL",
	}
	cli.HelpFlag = &cli.BoolFlag{
		Name:     "h",
		Aliases:  []string{"help"},
		Usage:    "Show help",
		Category: "GLOBAL",
	}
}

func (config *Configuration) Flags(elkuserOnly bool) []cli.Flag {
	// set the default config file name
	confDirPath := UserHomeDir() + string(os.PathSeparator) + confDir
	ConfFile = confDirPath + string(os.PathSeparator) + defaultConfFile

	var flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "c",
			Aliases:     []string{"config"},
			EnvVars:     []string{"ELKTAIL_CONFIG"},
			Value:       ConfFile,
			Usage:       "Configuration file - can be either .json or .yaml",
			Destination: &config.ConfigFile,
			Category:    "GLOBAL",
		},
		&cli.BoolFlag{
			Name:        "s",
			Aliases:     []string{"save"},
			Usage:       "Save query terms - next invocation of elktail (without parameters) will use saved query terms. Any additional terms specified will be applied with AND operator to saved terms",
			Destination: &config.SaveQuery,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "U",
			Aliases:     []string{"url"},
			EnvVars:     []string{"ELKTAIL_URL"},
			Value:       "http://localhost:9200",
			Usage:       "(*) ElasticSearch URL",
			Destination: &config.SearchTarget.Url,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "cacert",
			EnvVars:     []string{"ELKTAIL_CACERT"},
			Value:       "",
			Usage:       "(*) ca certificate to use when accessing via TLS",
			Destination: &config.SearchTarget.CaCert,
			Category:    "GLOBAL",
		},
		&cli.BoolFlag{
			Name:        "I",
			Aliases:     []string{"insecure"},
			Usage:       "Insecure skip verify server certificate",
			Destination: &config.SearchTarget.Insecure,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "cert",
			EnvVars:     []string{"ELKTAIL_CERT"},
			Value:       "",
			Usage:       "(*) certificate to use when accessing via TLS",
			Destination: &config.SearchTarget.Cert,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "key",
			EnvVars:     []string{"ELKTAIL_KEY"},
			Value:       "",
			Usage:       "(*) key to use when accessing via TLS",
			Destination: &config.SearchTarget.Key,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "apikey",
			EnvVars:     []string{"ELKTAIL_APIKEY"},
			Value:       "",
			Usage:       "(*) API key to use when accessing via TLS",
			Destination: &config.SearchTarget.APIKey,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "socks",
			Aliases:     []string{"socks-proxy"},
			EnvVars:     []string{"ELKTAIL_SOCKS_PROXY_URL"},
			Value:       "",
			Usage:       "(*) Use a socks proxy to connect. Format for the argument is <host>:<port> eg: localhost:6676 - note will automatically pick up HTTP_PROXY env var if this not set",
			Destination: &config.SocksProxy,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "ssh",
			Aliases:     []string{"ssh-tunnel"},
			EnvVars:     []string{"ELKTAIL_SSH_TUNNEL"},
			Value:       "",
			Usage:       "(*) Use ssh tunnel to connect. Format for the argument is [localport:][user@]sshhost.tld[:sshport]",
			Destination: &config.SSHTunnelParams,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "ssh-hop",
			Aliases:     []string{"ssh-tunnel-hop"},
			EnvVars:     []string{"ELKTAIL_SSH_HOP"},
			Value:       "",
			Usage:       "(*) second ssh hop - must be used incojunction with --ssh. Format for the argument is sshhost.tld[:sshport]",
			Destination: &config.SSHTunnelHop,
			Category:    "GLOBAL",
		},
		&cli.IntFlag{
			Name:        "w",
			Aliases:     []string{"tunnel-wait"},
			EnvVars:     []string{"ELKTAIL_SSH_WAIT"},
			Value:       1,
			Usage:       "(*) Number of seconds pause to enable tunnel to establish",
			Destination: &config.TunnelWait,
			Category:    "GLOBAL",
		},
		&cli.StringFlag{
			Name:        "u",
			Aliases:     []string{"user"},
			EnvVars:     []string{"ELKTAIL_USER"},
			Value:       "",
			Usage:       "(*) Username and password for authentication. curl-like format (separated by colon)",
			Destination: &config.User,
			Category:    "GLOBAL",
		},
		&cli.BoolFlag{
			Name:        "v1",
			Usage:       "Enable verbose output (for debugging)",
			Destination: &config.Verbose,
			Category:    "GLOBAL",
		},
		&cli.BoolFlag{
			Name:        "v2",
			Usage:       "Enable even more verbose output (for debugging)",
			Destination: &config.MoreVerbose,
			Category:    "GLOBAL",
		},
		&cli.BoolFlag{
			Name:        "v3",
			Usage:       "Same as v2 but also trace requests and responses (for debugging)",
			Destination: &config.TraceRequests,
			Category:    "GLOBAL",
		},
		cli.VersionFlag,
		cli.HelpFlag,
	}

	if !elkuserOnly {

		flags = append(flags, []cli.Flag{
			&cli.StringFlag{
				Name:        "F",
				Aliases:     []string{"format"},
				Value:       "[%@timestamp] %agent.name :: %message",
				Usage:       "(*) Message format for the entries - field names are referenced using % sign, for example '%@timestamp %message'",
				Destination: &config.QueryDefinition.Format,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "d",
				Aliases:     []string{"delimited"},
				Usage:       "Add a tab delimiter to output on field boundaries of --format",
				Destination: &config.Delimiter,
				Value:       false,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "f",
				Aliases:     []string{"follow"},
				Usage:       "Follow/stream result, like tail -f",
				Destination: &config.Follow,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "r",
				Aliases:     []string{"raw"},
				Usage:       "Output raw (JSON) records",
				Destination: &config.Raw,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "descending",
				Usage:       "Output rows in descending order",
				Destination: &config.Descending,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "l",
				Aliases:     []string{"lineno"},
				Usage:       "Output record line numbers",
				Destination: &config.LineNo,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "N",
				Aliases:     []string{"count"},
				Usage:       "Count records and exit",
				Destination: &config.Count,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "z",
				Aliases:     []string{"add-id"},
				Usage:       "Add record ID to output",
				Destination: &config.AddId,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "p",
				Aliases:     []string{"pretty-print"},
				Usage:       "Output raw pretty printed (JSON) records",
				Destination: &config.PrettyPrint,
				Category:    "GLOBAL",
			},
			&cli.StringFlag{
				Name:        "i",
				Aliases:     []string{"index-pattern"},
				Value:       "filebeat-\\d+\\.\\d+\\.\\d+.*",
				Usage:       "(*) Index pattern - elktail will attempt to tail only the latest of logstash's indexes matched by the pattern",
				Destination: &config.SearchTarget.IndexPattern,
				Category:    "GLOBAL",
			},
			&cli.StringFlag{
				Name:        "t",
				Aliases:     []string{"timestamp-field"},
				Value:       "@timestamp",
				Usage:       "(*) Timestamp field name used for tailing entries (default: @timestamp)",
				Destination: &config.QueryDefinition.TimestampField,
				Category:    "GLOBAL",
			},
			&cli.IntFlag{
				Name:        "n",
				Aliases:     []string{"number"},
				Value:       250,
				Usage:       "(*) Number of entries fetched initially",
				Destination: &config.InitialEntries,
				Category:    "GLOBAL",
			},
			&cli.StringFlag{
				Name:        "a",
				Aliases:     []string{"after"},
				Value:       "",
				Usage:       "List results after or equal to specified date/time (example: -a \"2022-06-17T00:00\" also takes datemath expressions eg: -a \"now-1d\")",
				Destination: &config.QueryDefinition.AfterDateTime,
				Category:    "GLOBAL",
			},
			&cli.StringFlag{
				Name:        "b",
				Aliases:     []string{"before"},
				Value:       "",
				Usage:       "List results before or equal to specified date/time (example: -b \"2022-06-17T23:59\" also takes datemath expressions eg: -a \"now-15m\")",
				Destination: &config.QueryDefinition.BeforeDateTime,
				Category:    "GLOBAL",
			},
			&cli.StringFlag{
				Name:        "C",
				Aliases:     []string{"context-time"},
				Value:       "",
				Usage:       "+/- context time span relative to --after (ignores --before, defaults rows to 10,000) - expressed as datemath expression, and selects all records using search criteria eg: -C \"15m\" is +/- 15 minutes",
				Destination: &config.QueryDefinition.ContextSpanTime,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "fields",
				Usage:       "Output mapping fields and exit.  Pass a single argument as a filter regex eg: --fields '^(agent|kubernetes)'",
				Destination: &config.PrintFields,
				Category:    "GLOBAL",
			},
			&cli.BoolFlag{
				Name:        "terms",
				Usage:       "Output query terms values in results",
				Destination: &config.PrintTerms,
				Category:    "GLOBAL",
			},
		}...)
	}

	return flags
}

func (c *Configuration) PrintCmdLine() string {
	c.Setup()

	var cmd = string("elktail")

	if c.SearchTarget.Url != "" {
		cmd += " --url " + c.SearchTarget.Url
	}
	// does not currently exist ??
	if c.SearchTarget.TunnelUrl != "" {
		cmd += " --TunnelUrl " + c.SearchTarget.TunnelUrl
	}
	if c.SocksProxy != "" {
		cmd += " --socks \"" + c.SocksProxy + "\""
	}
	if c.SSHTunnelHop != "" {
		cmd += " --ssh-tunnel-hop \"" + c.SSHTunnelHop + "\""
	}
	if c.SSHTunnelParams != "" {
		cmd += " --ssh-tunnel \"" + c.SSHTunnelParams + "\""
	}
	if c.User != "" {
		cmd += " --user \"" + c.User + "\""
	}
	if c.Password != "" {
		cmd += " --password \"" + c.Password + "\""
	}
	if c.TunnelWait > 1 {
		cmd += " --tunnel-wait " + fmt.Sprintf("%d", c.TunnelWait)
	}
	if c.SearchTarget.IndexPattern != "" {
		cmd += " --index-pattern \"" + c.SearchTarget.IndexPattern + "\""
	}
	if c.SearchTarget.CaCert != "" {
		cmd += " --cacert " + c.SearchTarget.CaCert
	}
	if c.SearchTarget.Insecure {
		cmd += " --insecure "
	}
	if c.SearchTarget.Cert != "" {
		cmd += " --cert " + c.SearchTarget.Cert
	}
	if c.SearchTarget.Key != "" {
		cmd += " --key " + c.SearchTarget.Key
	}
	if c.SearchTarget.APIKey != "" {
		cmd += " --apikey " + c.SearchTarget.APIKey
	}

	if c.QueryDefinition.Format != "" {
		cmd += " --format \"" + c.QueryDefinition.Format + "\""
	}
	if c.Descending {
		cmd += " --descending"
	}
	if c.LineNo {
		cmd += " --lineno"
	}
	if c.Count {
		cmd += " --count"
	}
	if c.AddId {
		cmd += " --add-id"
	}
	if c.PrettyPrint {
		cmd += " --pretty-print"
	}
	if c.Delimiter {
		cmd += " --delimited"
	}
	if c.Raw {
		cmd += " --raw"
	}
	if c.Follow {
		cmd += " --follow"
	}
	if c.QueryDefinition.TimestampField != "" {
		cmd += " --timestamp-field " + c.QueryDefinition.TimestampField
	}
	if c.InitialEntries > 0 {
		cmd += " --number " + fmt.Sprintf("%d", c.InitialEntries)
	}
	if c.QueryDefinition.AfterDateTime != "" {
		cmd += " --after \"" + c.QueryDefinition.AfterDateTime + "\""
	}
	if c.QueryDefinition.BeforeDateTime != "" {
		cmd += " --before \"" + c.QueryDefinition.BeforeDateTime + "\""
	}
	if c.QueryDefinition.ContextSpanTime != "" {
		cmd += " --context-time \"" + c.QueryDefinition.ContextSpanTime + "\""
	}
	if len(c.QueryDefinition.Terms) > 0 {
		cmd += " \"" + strings.Join(c.QueryDefinition.Terms, " ") + "\""
	}
	return cmd
}

func (c *Configuration) IsRaw() bool {
	return c.Raw || c.PrettyPrint
}

func (c *Configuration) IsPrettyPrint() bool {
	return c.PrettyPrint
}

// Elktail will work in list-only (no follow) mode if
// appropriate flag is set or
// if query has date-time filtering enabled
// if follow and startdate are not set together
func (c *Configuration) IsListOnly() bool {
	return !c.Follow || (c.QueryDefinition.IsDateTimeFiltered() && c.QueryDefinition.BeforeDateTime != "")
}

func (q *QueryDefinition) IsDateTimeFiltered() bool {
	return q.AfterDateTime != "" || q.ContextSpanTime != "" || q.BeforeDateTime != ""
}

func IsConfigRelevantFlagSet(c *cli.Context) bool {
	for _, flag := range configRelevantFlags {
		if c.IsSet(flag) {
			return true
		}
	}
	return false
}
