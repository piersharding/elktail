//go:build linux || darwin

/* Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

package configuration

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/piersharding/elktail/gocliselect"
)

func getConfigFileInteractive(confFile string) string {

	// check what the configuration file is - file or dir
	fileInfo, err := os.Stat(confFile)
	if err != nil {
		Error.Printf("Failed to stat configuration file/dir:%s - %s.\n", confFile, err)
		return ""
	}
	// if it is a directory, hunt for config files and present options
	if fileInfo.IsDir() {
		stat, _ := os.Stdin.Stat()
		if (stat.Mode() & os.ModeCharDevice) == 0 {
			Error.Fatalf("Configuration is dir but no terminal:%s.\n", confFile)
		}
		// list .json or .yaml files
		jsons, err := filepath.Glob(confFile + "/*.json")
		if err != nil {
			Error.Fatalf("Failed to glob configuration dir for .json: %s - %s.\n", confFile, err)
		}
		yamls, err := filepath.Glob(confFile + "/*.yaml")
		if err != nil {
			Error.Fatalf("Failed to glob configuration dir for .yaml: %s - %s.\n", confFile, err)
		}
		files := append(jsons, yamls...)

		// build menu and present
		menu := gocliselect.NewMenu("Choose config file (select and <enter>): ")
		menu.VimKeys = true
		for idx, f := range files {
			menu.AddItem(fmt.Sprintf("[%d] %s", idx, f), f)
		}
		confFile = menu.Display()
		if confFile == "" {
			Error.Fatalf("\n\nNo configuration file selected.\n")
		}
	}
	return confFile
}
