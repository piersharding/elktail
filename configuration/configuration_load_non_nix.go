//go:build !linux && !darwin

/* Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

package configuration

func getConfigFileInteractive(confFile string) string {

	// noop for non-unix builds
	return confFile
}
