/* Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * Based on blog post by Svett Ralchev: http://blog.ralch.com/tutorial/golang-ssh-tunneling/
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
package req

// "crypto/rsa"

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.com/piersharding/elktail/configuration"
	"gitlab.com/piersharding/elktail/logging"
	"golang.org/x/net/proxy"
)

var (
	Trace *log.Logger
	Info  *log.Logger
	Error *log.Logger
)

type Endpoint struct {
	Host string
	Port int
}

func (endpoint *Endpoint) String() string {
	return fmt.Sprintf("%s:%d", endpoint.Host, endpoint.Port)
}

type ElasticClient struct {
	Client *http.Client

	Config *configuration.Configuration
}

type ElasticRequest struct {
	Client *ElasticClient

	Request *http.Request
	Config  *configuration.Configuration
}

// configure a request
func BuildRequest(config *configuration.Configuration, url string, body string, method string) *http.Request {

	var req *http.Request
	var err error
	url = config.SearchTarget.Url + url
	if method == "" {
		// default to POST
		method = "POST"
	}
	if (method == "POST" || method == "PUT" || method == "DELETE") && body != "" {
		req, err = http.NewRequest(method, url, strings.NewReader(body))

	} else {
		// GET or empty body
		req, err = http.NewRequest(method, url, nil)
	}
	if err != nil {
		Error.Fatalf("Incorrect http.Request configuration: %s", err)
	}
	req.Header.Add("Content-Type", "application/json ")
	req.Header.Add("Accept", "application/json ")

	if config.User != "" {
		req.SetBasicAuth(config.User, config.Password)
	}
	Trace.Printf("http.Request body: %s", body)

	return req
}

// Add API Key to the http.Request
func AddApiKey(config *configuration.Configuration, req *http.Request) *http.Request {

	if config.SearchTarget.APIKey != "" {
		// blank out basic auth
		req.Header.Del("Authorization")
		// add the API Key for client auth
		req.Header.Add("Authorization", "ApiKey "+config.SearchTarget.APIKey)
		Trace.Printf("Added API Key: %s", config.SearchTarget.APIKey)
	}
	return req
}

// create an http client with optional mTLS configuration
func BuildClient(config *configuration.Configuration) *http.Client {
	Trace = logging.Trace
	Info = logging.Info
	Error = logging.Error

	var cacert = config.SearchTarget.CaCert
	var cert = config.SearchTarget.Cert
	var key = config.SearchTarget.Key
	var insecure = config.SearchTarget.Insecure
	var caCert []byte
	var httpClient http.Client
	if cacert != "" || (cert != "" && key != "") {
		tlsConfig := &tls.Config{InsecureSkipVerify: insecure}
		if cacert != "" {
			// Create a CA certificate pool and add cert.pem to it
			if len(cacert) > 1024 {
				Info.Printf("CaCert filename is too long (%s), attempting to use inline cert", cacert)
				caCert = []byte(cacert)
			} else {

				if _, err := os.Stat(cacert); errors.Is(err, os.ErrNotExist) {
					Info.Printf("CaCert file does not exist (%s), attempting to use inline cert", cacert)
					caCert = []byte(cacert)
				} else {
					caCert, err = os.ReadFile(cacert)
					if err != nil {
						Error.Fatalf("Bad ca certificate file: %s", err)
					}
				}
			}
			caCertPool := x509.NewCertPool()
			caCertPool.AppendCertsFromPEM(caCert)
			tlsConfig.RootCAs = caCertPool
		}

		if cert != "" && key != "" {
			// add the client cert for mTLS
			cert, err := tls.LoadX509KeyPair(cert, key)
			if err != nil {
				Error.Fatalf("Bad certificate and/or key: %s", err)
			}
			Info.Println("Set the TLS cert/key")
			tlsConfig.Certificates = []tls.Certificate{cert}
		}

		transport := &http.Transport{TLSClientConfig: tlsConfig}
		// check to see if env var is set and use that unless otherwise specified
		proxyHost := os.Getenv("HTTP_PROXY")
		url := strings.Split(proxyHost, "/")
		proxyHost = url[len(url)-1]

		if proxyHost != "" && config.SocksProxy == "" {
			config.SocksProxy = proxyHost
		}
		if config.SocksProxy != "" {
			Trace.Printf("Adding  socks5 proxy to Transport: %s", config.SocksProxy)
			// create a socks5 dialer
			dialer, err := proxy.SOCKS5("tcp", config.SocksProxy, nil, proxy.Direct)
			if err != nil {
				Error.Fatalf("can't connect to the proxy: %v", err)
			}
			dialContext := func(ctx context.Context, network, address string) (net.Conn, error) {
				return dialer.Dial(network, address)
			}
			// transport.Dial = dialer.Dial
			transport.DialContext = dialContext
		}

		httpClient = http.Client{Transport: transport, Timeout: 60 * time.Second}
	} else {
		httpClient = http.Client{Timeout: 60 * time.Second}
	}
	return &httpClient
}

// func (tunnel *Elastic) Start() error {
// 	Trace.Printf("SSH Tunnel:  Sever dialing to [%s] ...", tunnel.Server.String())

// 	start := time.Now()
// 	serverConn, err := ssh.Dial("tcp", tunnel.Server.String(), tunnel.Config)
// 	if err != nil {
// 		Error.Fatalf("SSH Tunnel: %s\n", err)
// 		return err
// 	}
// 	defer serverConn.Close()
// 	Info.Printf("SSH Tunnel: established serverConn [%s] ...", tunnel.Server.String())
// 	elapsed := time.Since(start)
// 	Trace.Printf("SSH Tunnel: took %s for ssh.Dial 1", elapsed)

// 	// If there is an intermeidate JumpHost then chain it in here
// 	if tunnel.Intermediate.Host != "" {
// 		Trace.Printf("SSH Tunnel:  Intermediate dialing to [%s] ...", tunnel.Intermediate.String())

// 		start = time.Now()
// 		intConn, err := serverConn.Dial("tcp", tunnel.Intermediate.String())
// 		if err != nil {
// 			Error.Fatalf("SSH Tunnel: connection to Intermediate failed [%s]: %s\n", tunnel.Intermediate.String(), err)
// 		}
// 		elapsed = time.Since(start)
// 		Trace.Printf("SSH Tunnel: took %s for serverConn.Dial", elapsed)
// 		start = time.Now()
// 		ncc, chans, reqs, err := ssh.NewClientConn(intConn, tunnel.Intermediate.String(), tunnel.Config)
// 		if err != nil {
// 			Error.Fatalf("SSH Tunnel: ssh.NewClientConn for JumpHost failed [%s]: %s\n", tunnel.Intermediate.String(), err)
// 		}
// 		elapsed = time.Since(start)
// 		Trace.Printf("SSH Tunnel: took %s for ssh.NewClientConn", elapsed)
// 		//
// 		start = time.Now()
// 		serverConn = ssh.NewClient(ncc, chans, reqs)
// 		elapsed = time.Since(start)
// 		Trace.Printf("SSH Tunnel: took %s for ssh.NewClient", elapsed)
// 		Info.Printf("SSH Tunnel: established Intermediate [%s] ...", tunnel.Intermediate.String())
// 	} else {
// 		Trace.Printf("SSH Tunnel: NO Intermediate [%s] ...", tunnel.Intermediate.String())
// 	}

// 	Trace.Printf("SSH Tunnel:  Remote dialing to [%s] ...", tunnel.Remote.String())
// 	remoteConn, err := serverConn.Dial("tcp", tunnel.Remote.String())
// 	// remoteConn, err := serverConn.Dial("tcp", "192.168.99.131:9200")
// 	if err != nil {
// 		Error.Fatalf("SSH Tunnel: Remote dial error to [%s]: %s\n    NOTE: you need to specify --url with --ssh\n", tunnel.Remote.String(), err)
// 		return err
// 	}
// 	defer remoteConn.Close()
// 	Info.Printf("SSH Tunnel: established remoteConn [%s] ...", tunnel.Remote.String())

// 	Trace.Printf("SSH Tunnel:  net.Listen [%s] ...", tunnel.Local.String())
// 	listener, err := net.Listen("tcp", tunnel.Local.String())
// 	if err != nil {
// 		Error.Printf("SSH Tunnel: Failed to configure local server at %s. Error: %s", tunnel.Local.String(), err)
// 		return err
// 	}
// 	defer listener.Close()
// 	Info.Printf("SSH Tunnel: established listener [%s] ...", tunnel.Local.String())

// 	for {
// 		start = time.Now()
// 		localConn, err := listener.Accept()
// 		if err != nil {
// 			Error.Printf("SSH Tunnel: Failed to accept local connection: %s", err)
// 			return err
// 		}
// 		elapsed = time.Since(start)
// 		Trace.Printf("SSH Tunnel: took %s for listener.Accept", elapsed)

// 		Info.Printf("SSH Tunnel: Accepted connection to forward to the tunnel [%s]...", tunnel.Server.String())
// 		start = time.Now()

// 		copyConn := func(writer net.Conn, reader net.Conn) {
// 			_, err := io.Copy(writer, reader)
// 			if err != nil {
// 				Error.Fatalf("SSH Tunnel: Could not forward connection: %s\n", err)
// 			}
// 		}

// 		go copyConn(localConn, remoteConn)
// 		go copyConn(remoteConn, localConn)

// 		elapsed = time.Since(start)
// 		Trace.Printf("SSH Tunnel: took %s for tunnel.forward", elapsed)
// 	}
// }

// func SSHAgent() ssh.AuthMethod {
// 	if sshAgent, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK")); err == nil {
// 		return ssh.PublicKeysCallback(agent.NewClient(sshAgent).Signers)
// 	}
// 	return nil
// }

// func GetUser() (string, error) {
// 	//attempt fetching logged in user via os/user package. It may not work when cross-compiled due to CGO requirement.
// 	//More info at: https://github.com/golang/go/issues/11797
// 	osUser, err := user.Current()
// 	Trace.Printf("os/user detected user as %v", osUser)
// 	if err != nil || osUser == nil || osUser.Username == "" {
// 		// os/user didn't work. Let's try using "whoami" command
// 		path, err := exec.LookPath("whoami")
// 		if err != nil {
// 			//whoami not found... we're giving up
// 			return "", errors.New("Could not detect current user")
// 		}
// 		out, err := exec.Command(path).Output()
// 		if err != nil {
// 			//something went wrong, giving up
// 			return "", errors.New("Could not detect current user")
// 		}
// 		return strings.TrimSpace(string(out)), nil
// 	}
// 	return osUser.Username, nil
// }

// // sshHostDef user@sshhost.tld:port
// // tunnelDef  local_port:remote_host:remote_port
// func NewElasticFromHostStrings(sshHostDef string, sshHopDef string, tunnelDef string) *Elastic {

// 	Trace = logging.Trace
// 	Info = logging.Info
// 	Error = logging.Error

// 	sshHostRegexp := regexp.MustCompile(`((\d+):)?(([\w]+)@)?([\w\.\-]+)(:(\d{2,5}))?`)
// 	match := sshHostRegexp.FindAllStringSubmatch(sshHostDef, -1)
// 	if len(match) == 0 {
// 		Error.Fatalf("SSH Tunnel: Failed to parse ssh host %s\n", sshHostDef)
// 	}
// 	result := match[0]
// 	sshUser := result[4]
// 	sshHost := result[5]
// 	sshPort := parsePort(result[7], 22)
// 	Trace.Printf("sshUser:[%s], sshHost:[%s], sshPort:[%d]", sshUser, sshHost, sshPort)
// 	if sshUser == "" {
// 		osUser, err := GetUser()
// 		if err != nil {
// 			Error.Printf("Could not detect current username to use when connecting via SSH. Please specify a username "+
// 				"when specifying SSH host (e.g. your_username@%s)\n", sshHost)
// 			os.Exit(1)
// 		}
// 		sshUser = osUser
// 	}

// 	Trace.Printf("SSH Tunnel: Server - User: %s, Host: %s, Port: %d\n", sshUser, sshHost, sshPort)

// 	sshHopHost := ""
// 	sshHopPort := 0
// 	if sshHopDef != "" {
// 		match = sshHostRegexp.FindAllStringSubmatch(sshHopDef, -1)
// 		if len(match) == 0 {
// 			Error.Fatalf("SSH Tunnel: Failed to parse ssh hop %s\n", sshHostDef)
// 		}
// 		result = match[0]
// 		sshHopHost = result[5]
// 		sshHopPort = parsePort(result[7], 22)
// 		Trace.Printf("sshHopHost:[%s], sshHopPort:[%d]", sshHopHost, sshHopPort)
// 	}

// 	//Setting up defaults
// 	localPort := 9199
// 	remotePort := 9200
// 	remoteHost := "localhost"

// 	tunnelRegexp := regexp.MustCompile(`((\d{2,5}):)?([^:@]+)(:(\d{2,5}))?`)
// 	match = tunnelRegexp.FindAllStringSubmatch(tunnelDef, -1)
// 	if len(match) == 0 {
// 		Trace.Printf("SSH Tunnel: Failed to parse [%s] remote tunnel host/port, using defaults\n", tunnelDef)
// 	} else {
// 		result = match[0]
// 		localPort = parsePort(result[2], 9199)
// 		remotePort = parsePort(result[5], 9200)
// 		remoteHost = result[3]
// 	}

// 	Trace.Printf("SSH Tunnel: Local port : %d, Remote Host: %s, Remote Port: %d\n", localPort, remoteHost, remotePort)

// 	return NewElastic(sshUser, sshHost, sshPort, sshHopHost, sshHopPort, localPort, remoteHost, remotePort)
// }

// func parsePort(portStr string, defaultPort int) int {
// 	if portStr != "" {
// 		port, err := strconv.Atoi(portStr)
// 		if err != nil {
// 			Error.Printf("SSH Tunnel: Reverting to port %d because given port was not numeric: %s\n", defaultPort, err)
// 			port = defaultPort
// 		}
// 		return port
// 	}
// 	return defaultPort
// }

// // Read password from the console
// func readPasswd() string {
// 	bytePassword, err := terminal.ReadPassword(0)
// 	if err != nil {
// 		Error.Fatalln("Failed to read password.")
// 	}
// 	fmt.Println()
// 	return string(bytePassword)
// }

// func passwordCallback() (string, error) {
// 	fmt.Println("Enter ssh password:")
// 	pwd := readPasswd()
// 	return pwd, nil
// }

// func hostKeyCallback(hostname string, remote net.Addr, key ssh.PublicKey) error {
// 	Trace.Printf("host: %s", hostname)
// 	return nil
// }

// func NewElastic(sshUser string, sshHost string, sshPort int, sshHopHost string, sshHopPort int, localPort int,
// 	remoteHost string, remotePort int) *Elastic {
// 	localEndpoint := &Endpoint{
// 		Host: "localhost",
// 		Port: localPort,
// 	}

// 	serverEndpoint := &Endpoint{
// 		Host: sshHost,
// 		Port: sshPort,
// 	}

// 	hopEndpoint := &Endpoint{
// 		Host: sshHopHost,
// 		Port: sshHopPort,
// 	}

// 	remoteEndpoint := &Endpoint{
// 		Host: remoteHost,
// 		Port: remotePort,
// 	}

// 	// add ssh keyfile - example:
// 	// https://gist.github.com/stefanprodan/2d20d0c6fdab6f14ce8219464e8b4b9a#file-go-ssh-encrypted-pem-go-L18

// 	sshConfig := &ssh.ClientConfig{
// 		User: sshUser,
// 		Auth: []ssh.AuthMethod{
// 			SSHAgent(),
// 			ssh.PasswordCallback(passwordCallback),
// 		},
// 		HostKeyCallback: ssh.HostKeyCallback(hostKeyCallback),
// 		Timeout:         30 * time.Second,
// 	}

// 	return &Elastic{
// 		Config:       sshConfig,
// 		Local:        localEndpoint,
// 		Server:       serverEndpoint,
// 		Remote:       remoteEndpoint,
// 		Intermediate: hopEndpoint,
// 	}
// }
