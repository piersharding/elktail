/* Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * Based on blog post by Svett Ralchev: http://blog.ralch.com/tutorial/golang-ssh-tunneling/
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
package ssh

// "crypto/rsa"

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"os/user"
	"regexp"
	"strconv"
	"strings"
	"time"

	logging "gitlab.com/piersharding/elktail/logging"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"golang.org/x/crypto/ssh/terminal"
)

var (
	Trace *log.Logger
	Info  *log.Logger
	Error *log.Logger
)

type Endpoint struct {
	Host string
	Port int
}

func (endpoint *Endpoint) String() string {
	return fmt.Sprintf("%s:%d", endpoint.Host, endpoint.Port)
}

type SSHTunnel struct {
	Local        *Endpoint
	Server       *Endpoint
	Remote       *Endpoint
	Intermediate *Endpoint

	Config    *ssh.ClientConfig
	HopConfig *ssh.ClientConfig
}

func (tunnel *SSHTunnel) Start() error {
	Trace.Printf("SSH Tunnel:  Sever dialing to [%s] ...", tunnel.Server.String())

	start := time.Now()
	serverConn, err := ssh.Dial("tcp", tunnel.Server.String(), tunnel.Config)
	if err != nil {
		Error.Fatalf("SSH Tunnel: %s\n", err)
		return err
	}
	defer serverConn.Close()
	Info.Printf("SSH Tunnel: established serverConn [%s] ...", tunnel.Server.String())
	elapsed := time.Since(start)
	Trace.Printf("SSH Tunnel: took %s for ssh.Dial 1", elapsed)

	// If there is an intermeidate JumpHost then chain it in here
	if tunnel.Intermediate.Host != "" {
		Trace.Printf("SSH Tunnel:  Intermediate dialing to [%s] ...", tunnel.Intermediate.String())

		start = time.Now()
		intConn, err := serverConn.Dial("tcp", tunnel.Intermediate.String())
		if err != nil {
			Error.Fatalf("SSH Tunnel: connection to Intermediate failed [%s]: %s\n", tunnel.Intermediate.String(), err)
		}
		elapsed = time.Since(start)
		Trace.Printf("SSH Tunnel: took %s for serverConn.Dial", elapsed)
		start = time.Now()
		ncc, chans, reqs, err := ssh.NewClientConn(intConn, tunnel.Intermediate.String(), tunnel.HopConfig)
		if err != nil {
			Error.Fatalf("SSH Tunnel: ssh.NewClientConn for JumpHost failed [%s]: %s\n", tunnel.Intermediate.String(), err)
		}
		elapsed = time.Since(start)
		Trace.Printf("SSH Tunnel: took %s for ssh.NewClientConn", elapsed)
		//
		start = time.Now()
		serverConn = ssh.NewClient(ncc, chans, reqs)
		elapsed = time.Since(start)
		Trace.Printf("SSH Tunnel: took %s for ssh.NewClient", elapsed)
		Info.Printf("SSH Tunnel: established Intermediate [%s] ...", tunnel.Intermediate.String())
	} else {
		Trace.Printf("SSH Tunnel: NO Intermediate [%s] ...", tunnel.Intermediate.String())
	}

	Trace.Printf("SSH Tunnel:  Remote dialing to [%s] ...", tunnel.Remote.String())
	remoteConn, err := serverConn.Dial("tcp", tunnel.Remote.String())
	// remoteConn, err := serverConn.Dial("tcp", "192.168.99.131:9200")
	if err != nil {
		Error.Fatalf("SSH Tunnel: Remote dial error to [%s]: %s\n    NOTE: you need to specify --url with --ssh\n", tunnel.Remote.String(), err)
		return err
	}
	defer remoteConn.Close()
	Info.Printf("SSH Tunnel: established remoteConn [%s] ...", tunnel.Remote.String())

	Trace.Printf("SSH Tunnel:  net.Listen [%s] ...", tunnel.Local.String())
	listener, err := net.Listen("tcp", tunnel.Local.String())
	if err != nil {
		Error.Printf("SSH Tunnel: Failed to configure local server at %s. Error: %s", tunnel.Local.String(), err)
		return err
	}
	defer listener.Close()
	Info.Printf("SSH Tunnel: established listener [%s] ...", tunnel.Local.String())

	for {
		start = time.Now()
		localConn, err := listener.Accept()
		if err != nil {
			Error.Printf("SSH Tunnel: Failed to accept local connection: %s", err)
			return err
		}
		elapsed = time.Since(start)
		Trace.Printf("SSH Tunnel: took %s for listener.Accept", elapsed)

		Info.Printf("SSH Tunnel: Accepted connection to forward to the tunnel [%s]...", tunnel.Server.String())
		start = time.Now()

		copyConn := func(writer net.Conn, reader net.Conn) {
			_, err := io.Copy(writer, reader)
			if err != nil {
				Error.Fatalf("SSH Tunnel: Could not forward connection: %s\n", err)
			}
		}

		go copyConn(localConn, remoteConn)
		go copyConn(remoteConn, localConn)

		elapsed = time.Since(start)
		Trace.Printf("SSH Tunnel: took %s for tunnel.forward", elapsed)
	}
}

func SSHAgent() ssh.AuthMethod {
	if sshAgent, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK")); err == nil {
		return ssh.PublicKeysCallback(agent.NewClient(sshAgent).Signers)
	}
	return nil
}

func GetUser() (string, error) {
	//attempt fetching logged in user via os/user package. It may not work when cross-compiled due to CGO requirement.
	//More info at: https://github.com/golang/go/issues/11797
	osUser, err := user.Current()
	Trace.Printf("os/user detected user as %v", osUser)
	if err != nil || osUser == nil || osUser.Username == "" {
		// os/user didn't work. Let's try using "whoami" command
		path, err := exec.LookPath("whoami")
		if err != nil {
			//whoami not found... we're giving up
			return "", errors.New("Could not detect current user")
		}
		out, err := exec.Command(path).Output()
		if err != nil {
			//something went wrong, giving up
			return "", errors.New("Could not detect current user")
		}
		return strings.TrimSpace(string(out)), nil
	}
	return osUser.Username, nil
}

// sshHostDef user@sshhost.tld:port
// tunnelDef  local_port:remote_host:remote_port
func NewSSHTunnelFromHostStrings(sshHostDef string, sshHopDef string, tunnelDef string) *SSHTunnel {

	Trace = logging.Trace
	Info = logging.Info
	Error = logging.Error

	sshHostRegexp := regexp.MustCompile(`((\d+):)?(([\w][\w\d\_\.]+)@)?([\w\.\-]+)(:(\d{2,5}))?`)
	match := sshHostRegexp.FindAllStringSubmatch(sshHostDef, -1)
	if len(match) == 0 {
		Error.Fatalf("SSH Tunnel: Failed to parse ssh host %s\n", sshHostDef)
	}
	result := match[0]
	sshUser := result[4]
	sshHost := result[5]
	sshPort := parsePort(result[7], 22)
	Trace.Printf("sshUser:[%s], sshHost:[%s], sshPort:[%d]", sshUser, sshHost, sshPort)
	if sshUser == "" {
		osUser, err := GetUser()
		if err != nil {
			Error.Printf("Could not detect current username to use when connecting via SSH. Please specify a username "+
				"when specifying SSH host (e.g. your_username@%s)\n", sshHost)
			os.Exit(1)
		}
		sshUser = osUser
	}

	Trace.Printf("SSH Tunnel: Server - User: %s, Host: %s, Port: %d\n", sshUser, sshHost, sshPort)

	sshHopUser := ""
	sshHopHost := ""
	sshHopPort := 0
	if sshHopDef != "" {
		match = sshHostRegexp.FindAllStringSubmatch(sshHopDef, -1)
		if len(match) == 0 {
			Error.Fatalf("SSH Tunnel: Failed to parse ssh hop %s\n", sshHostDef)
		}
		result = match[0]
		sshHopUser = result[4]
		sshHopHost = result[5]
		sshHopPort = parsePort(result[7], 22)
		Trace.Printf("sshHopUser:[%s], sshHopHost:[%s], sshHopPort:[%d]", sshHopUser, sshHopHost, sshHopPort)
	}

	//Setting up defaults
	localPort := 9199
	remotePort := 9200
	remoteHost := "localhost"

	tunnelRegexp := regexp.MustCompile(`((\d{2,5}):)?([^:@]+)(:(\d{2,5}))?`)
	match = tunnelRegexp.FindAllStringSubmatch(tunnelDef, -1)
	if len(match) == 0 {
		Trace.Printf("SSH Tunnel: Failed to parse [%s] remote tunnel host/port, using defaults\n", tunnelDef)
	} else {
		result = match[0]
		localPort = parsePort(result[2], 9199)
		remotePort = parsePort(result[5], 9200)
		remoteHost = result[3]
	}

	Trace.Printf("SSH Tunnel: Local port : %d, Remote Host: %s, Remote Port: %d\n", localPort, remoteHost, remotePort)

	return NewSSHTunnel(sshUser, sshHost, sshPort, sshHopUser, sshHopHost, sshHopPort, localPort, remoteHost, remotePort)
}

func parsePort(portStr string, defaultPort int) int {
	if portStr != "" {
		port, err := strconv.Atoi(portStr)
		if err != nil {
			Error.Printf("SSH Tunnel: Reverting to port %d because given port was not numeric: %s\n", defaultPort, err)
			port = defaultPort
		}
		return port
	}
	return defaultPort
}

// Read password from the console
func readPasswd() string {
	bytePassword, err := terminal.ReadPassword(0)
	if err != nil {
		Error.Fatalln("Failed to read password.")
	}
	fmt.Println()
	return string(bytePassword)
}

func passwordCallback() (string, error) {
	fmt.Println("Enter ssh password:")
	pwd := readPasswd()
	return pwd, nil
}

func hostKeyCallback(hostname string, remote net.Addr, key ssh.PublicKey) error {
	Trace.Printf("host: %s", hostname)
	return nil
}

func NewSSHTunnel(sshUser string, sshHost string, sshPort int, sshHopUser string, sshHopHost string, sshHopPort int, localPort int,
	remoteHost string, remotePort int) *SSHTunnel {
	localEndpoint := &Endpoint{
		Host: "localhost",
		Port: localPort,
	}

	serverEndpoint := &Endpoint{
		Host: sshHost,
		Port: sshPort,
	}

	hopEndpoint := &Endpoint{
		Host: sshHopHost,
		Port: sshHopPort,
	}

	remoteEndpoint := &Endpoint{
		Host: remoteHost,
		Port: remotePort,
	}

	// add ssh keyfile - example:
	// https://gist.github.com/stefanprodan/2d20d0c6fdab6f14ce8219464e8b4b9a#file-go-ssh-encrypted-pem-go-L18

	sshConfig := &ssh.ClientConfig{
		User: sshUser,
		Auth: []ssh.AuthMethod{
			SSHAgent(),
			ssh.PasswordCallback(passwordCallback),
		},
		HostKeyCallback: ssh.HostKeyCallback(hostKeyCallback),
		Timeout:         30 * time.Second,
	}

	sshHopConfig := &ssh.ClientConfig{
		User: sshHopUser,
		Auth: []ssh.AuthMethod{
			SSHAgent(),
			ssh.PasswordCallback(passwordCallback),
		},
		HostKeyCallback: ssh.HostKeyCallback(hostKeyCallback),
		Timeout:         30 * time.Second,
	}

	return &SSHTunnel{
		Config:       sshConfig,
		HopConfig:    sshHopConfig,
		Local:        localEndpoint,
		Server:       serverEndpoint,
		Remote:       remoteEndpoint,
		Intermediate: hopEndpoint,
	}
}
