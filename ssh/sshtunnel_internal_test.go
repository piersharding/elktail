/* Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
package ssh

import (
	"os"
	"testing"

	logging "gitlab.com/piersharding/elktail/logging"
	"gitlab.com/piersharding/elktail/testutils"
)

func TestNewSSHTunnelFromHostStrings(t *testing.T) {
	logging.InitLogging(os.Stderr, os.Stderr, os.Stderr, true)
	tunnel := NewSSHTunnelFromHostStrings("knesek@test1.example.com:2222", "test1.example.com:2222", "9200:localhost:9200")
	testutils.AssertEqualsString(t, tunnel.Server.Host, "test1.example.com")
	testutils.AssertEqualsString(t, tunnel.Intermediate.Host, "test1.example.com")
	testutils.AssertEqualsString(t, tunnel.Remote.Host, "localhost")
	testutils.AssertEqualsInt(t, tunnel.Server.Port, 2222)
	testutils.AssertEqualsInt(t, tunnel.Intermediate.Port, 2222)
	testutils.AssertEqualsInt(t, tunnel.Remote.Port, 9200)
	testutils.AssertEqualsInt(t, tunnel.Local.Port, 9200)

	tunnel = NewSSHTunnelFromHostStrings("test1.example.com:2222", "", "")
	testutils.AssertEqualsString(t, tunnel.Server.Host, "test1.example.com")
	testutils.AssertEqualsInt(t, tunnel.Server.Port, 2222)

	tunnel = NewSSHTunnelFromHostStrings("test1.example.com:2222", "test1.example.com:2222", "")
	testutils.AssertEqualsString(t, tunnel.Server.Host, "test1.example.com")
	testutils.AssertEqualsInt(t, tunnel.Server.Port, 2222)
	testutils.AssertEqualsString(t, tunnel.Intermediate.Host, "test1.example.com")
	testutils.AssertEqualsInt(t, tunnel.Intermediate.Port, 2222)

	tunnel = NewSSHTunnelFromHostStrings("knesek@test1.example.com", "", "")
	testutils.AssertEqualsString(t, tunnel.Server.Host, "test1.example.com")
	testutils.AssertEqualsInt(t, tunnel.Server.Port, 22)

	tunnel = NewSSHTunnelFromHostStrings("test1.example.com", "", "")
	testutils.AssertEqualsString(t, tunnel.Server.Host, "test1.example.com")
	testutils.AssertEqualsInt(t, tunnel.Server.Port, 22)

}
