/*
 * Copyright (C) 2016 Krešimir Nesek
 * Copyright (C) 2021 Piers Harding
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

package tail

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/PaesslerAG/jsonpath"
	"gitlab.com/piersharding/elktail/elastic"

	datemath "github.com/timberio/go-datemath"
	"github.com/urfave/cli/v2"
	color "gitlab.com/piersharding/elktail/color"
	configuration "gitlab.com/piersharding/elktail/configuration"
	logging "gitlab.com/piersharding/elktail/logging"
	request "gitlab.com/piersharding/elktail/req"
)

var (
	Trace *log.Logger
	Info  *log.Logger
	Error *log.Logger
)

// Tail is a structure that holds data necessary to perform tailing.
type Tail struct {
	// client          *elastic.Client                //elastic search client that we'll use to contact EL
	httpClient      *http.Client                   // the HTTP client for elastic calls
	configuration   *configuration.Configuration   // full runtime configuration
	queryDefinition *configuration.QueryDefinition //structure containing query definition and formatting
	indexPattern    string                         // index pattern for selecting indices
	indices         []string                       //indices to search through
	lastTimeStamp   string                         //timestamp of the last result
	lastIDs         []displayedEntry               //result IDs that we fetched in the last query, used to avoid duplicates when using tailing query time window
	order           bool                           //search order - true = ascending (may be reversed in case date-after filtering)
	raw             bool                           // Raw output
	count           bool                           // Count records and exit
	addId           bool                           // Add record ID to output
	prettyPrint     bool                           // Pretty Printed Raw output
	printTerms      bool                           // Print search terms in output
	delimiter       bool                           // Field delimiter
	verbose         bool                           // verbose flag
	moreVerbose     bool                           // more verbose flag
	trace           bool                           // trace flag
	first           bool                           // is this the first output record printed
	lineno          bool                           // print line numbers
	initialEntries  int                            // -n number of records
	idx             int                            // counter for records
	totalCount      int                            // running count for --count
}

type displayedEntry struct {
	timeStamp string
	id        string
}

func (entry *displayedEntry) isBefore(timeStamp string) bool {
	return entry.timeStamp < timeStamp
}

const dateFormatDMY = "2006-01-02"
const dateFormatFull = "2006-01-02T15:04:05.999Z07:00"
const tailingTimeWindow = 500

// NewTail creates a new Tailer using configuration
func NewTail(configuration *configuration.Configuration, ctx *cli.Context) *Tail {

	Trace = logging.Trace
	Info = logging.Info
	Error = logging.Error

	tail := new(Tail)
	tail.configuration = configuration

	var url = configuration.SearchTarget.Url
	if !strings.HasPrefix(url, "http") {
		url = "http://" + url
		Trace.Printf("Adding http:// prefix to given url. Url: " + url)
	}

	if !Must(regexp.MatchString(".*:\\d+", url)) && Must(regexp.MatchString("http://[^/]+$", url)) {
		url += ":9200"
		Trace.Printf("No port was specified, adding default port 9200 to given url. Url: %s", url)
	}

	//if a tunnel is successfully created, we need to connect to tunnel url (which is localhost on tunnel port)
	if configuration.SearchTarget.TunnelUrl != "" {
		url = configuration.SearchTarget.TunnelUrl
	}
	Trace.Printf("HTTP Client: URL [%s] ...", url)

	tail.httpClient = request.BuildClient(configuration)
	tail.queryDefinition = &configuration.QueryDefinition
	tail.raw = configuration.Raw
	tail.count = configuration.Count
	tail.addId = configuration.AddId
	tail.prettyPrint = configuration.PrettyPrint
	tail.printTerms = configuration.PrintTerms
	tail.lineno = configuration.LineNo

	tail.delimiter = configuration.Delimiter
	tail.verbose = configuration.Verbose
	tail.moreVerbose = configuration.MoreVerbose
	tail.trace = configuration.TraceRequests
	tail.indexPattern = configuration.SearchTarget.IndexPattern

	tail.indices = []string{}
	tail.selectIndices()
	// fix the intial entries based on whether the number was set
	tail.initialEntries = configuration.InitialEntries
	// if it is out of bounds then set it to maximum
	if configuration.InitialEntries <= 0 || configuration.InitialEntries > 10000 || configuration.Count {
		tail.initialEntries = 10000
	} else {
		// if it's a date range then let the date range determine how many rows up to maximum
		if !ctx.IsSet("n") {
			if (configuration.QueryDefinition.BeforeDateTime != "" && configuration.QueryDefinition.AfterDateTime != "") || configuration.QueryDefinition.ContextSpanTime != "" {
				tail.initialEntries = 10000
			}
		}
	}
	Trace.Printf("Line limit set to: %d", tail.initialEntries)

	//If we're date filtering on start date, then the sort needs to be ascending
	// but only if we don't explicitly follow
	tail.order = configuration.Descending
	if configuration.Follow {
		tail.order = false // should always be ascending
	}
	tail.first = true
	return tail
}

// Accessor for the indicies
func (tail *Tail) Indicies() []string {
	return tail.indices
}

// Are we at greater than or equal to verbose
func (tail *Tail) IsVebose() bool {
	return tail.verbose || tail.moreVerbose || tail.trace
}

func parseDateTime(datetime string, which string) string {
	if datetime == "" {
		return formatElasticTimeStamp(time.Now())
	}
	// parse date string as date math eg: now+1 etc.
	datetime_t, err := datemath.Parse(datetime)
	if err == nil {
		Trace.Printf("Parsed %s: %v ", which, datetime_t)
		datetime = fmt.Sprint(formatElasticTimeStamp(datetime_t.Time()))
	} else {
		Error.Fatalf("Could not parse %s [%v] - %v", which, datetime, err)
	}
	return datetime
}

// Selects appropriate indices in EL based on configuration. This basically means that if query is date filtered,
// then it attempts to select indices in the filtered date range, otherwise it selects the last index.
func (tail *Tail) selectIndices() {
	tail.indices = tail.indices[:0] // empty array
	var url = ""
	if strings.HasPrefix(tail.configuration.SearchTarget.IndexPattern, "filebeat") {
		url += "/_cat/indices/filebeat*?format=json"
	} else {
		url += "/_cat/indices?format=json"
	}
	Trace.Printf("Request URL: %s", url)

	req := request.AddApiKey(tail.configuration, request.BuildRequest(tail.configuration, url, "", "GET"))
	Trace.Printf("Req: %v", req)

	start := time.Now()
	resp, err := tail.httpClient.Do(req)
	if err != nil {
		Info.Println("Could not fetch available indices. Using pattern instead.", err)
		tail.indices = []string{tail.indexPattern}
		return
	}
	elapsed := time.Since(start)
	Trace.Printf("selectIndices: CatIndices took %s", elapsed)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("http.Request body read failed: %s", err)
	}

	Trace.Printf("Resp: %v", resp)
	Trace.Printf("Indicies: %s", string(body))

	var indicies_list []map[string]interface{}
	err = json.Unmarshal(body, &indicies_list)
	if err != nil {
		Trace.Printf("ERR [Unmarshal] %s", err)
		panic(err)
	}

	Trace.Printf("Obj: %v", indicies_list)
	indices := make([]string, len(indicies_list))
	date_part := "^" + strings.TrimSuffix(tail.indexPattern, ".*") + "(.*)$"
	Trace.Printf("selectIndices: regex for date %s", date_part)
	var date_part_regex = regexp.MustCompile(date_part)
	var rIndex = regexp.MustCompile(`^(?P<Group>(?P<Stub>.*?)-(?P<Version>(?P<Major>\d{1,2})\.(?P<Minor>\d{1,2})\.(?P<Patch>\d{1,2})))-(?P<Year>\d{4})\.(?P<Month>\d{2})\.(?P<Day>\d{2})-(?P<Instance>\d{6})$`)

	for i, indice := range indicies_list {
		index, ok := indice["index"]
		if ok {
			indices[i] = index.(string)
		}
	}

	// accumulate indicies groups
	indexGroups := make(map[string][]string)
	for _, idx := range indices {
		res := rIndex.FindStringSubmatch(idx)
		Trace.Printf("selectIndices: idx %s has: %s", idx, res)
		if len(res) > 0 {
			groupIndex := rIndex.SubexpIndex("Group")
			group := indexGroups[res[groupIndex]]
			if indexGroups[res[groupIndex]] == nil {
				indexGroups[res[groupIndex]] = []string{idx}
			} else {
				group = append(group, idx)
				indexGroups[res[groupIndex]] = group
			}
		}
	}
	Trace.Printf("indexGroups: %v", indexGroups)

	for _, indices := range indexGroups {

		sort.SliceStable(indices, func(i, j int) bool {
			a := date_part_regex.ReplaceAllString(indices[i], `$1`)
			b := date_part_regex.ReplaceAllString(indices[j], `$1`)
			return a < b
		})
		tail.indices = append(tail.indices, indices...)
	}
	Trace.Printf("Using indices: %s", tail.indices)

	if tail.queryDefinition.IsDateTimeFiltered() {
		startDate := tail.queryDefinition.AfterDateTime
		contextTime := tail.queryDefinition.ContextSpanTime
		endDate := tail.queryDefinition.BeforeDateTime
		Trace.Printf("Before parse StartDate: %v and ContextTime: %v and EndDate: %v", startDate, contextTime, endDate)
		// parse date string as date math eg: now+1 etc.
		if contextTime != "" {
			Trace.Printf("Using ContextTime: %v", contextTime)
			startDate = parseDateTime(tail.queryDefinition.AfterDateTime+"||-"+contextTime, "StartContextTime")
			endDate = parseDateTime(tail.queryDefinition.AfterDateTime+"||+"+contextTime, "EndContextTime")
		} else {
			// if startDate is empty then set it to epoch
			if startDate == "" {
				startDate = "1970-01-01"
			}
			startDate = parseDateTime(startDate, "StartDate")
			endDate = parseDateTime(endDate, "EndDate")
		}
		if endDate == "" {
			endDate = time.Now().Format(dateFormatDMY)
		}
		tail.queryDefinition.AfterDateTime = startDate
		tail.queryDefinition.BeforeDateTime = endDate
		Info.Printf("StartDate: %v and ContextTime: %v and EndDate: %v", startDate, contextTime, endDate)
	}
	// close idle connections on custom httpClient - but not for ssh tunnels
	if tail.httpClient != nil {
		tail.httpClient.CloseIdleConnections()
	}
}

// Start the tailer
func (tail *Tail) Start(follow bool) error {

	// bracket the pretty printed results
	if tail.prettyPrint {
		fmt.Println("[")
	}
	// delay := 500 * time.Millisecond
	delay := 0 * time.Millisecond
	unsuccessfull_search := 0
	incomplete := true
	for follow || incomplete {
		// lets just stop if we don't seem to be getting anything
		if unsuccessfull_search > 50 {
			Info.Printf("Exit after 50 unsuccessful searches - go back, reread list of indices\n")
			return nil
		}

		if unsuccessfull_search%10 == 0 && unsuccessfull_search > 0 {
			Trace.Printf("Refreshing indicies: %d\n", unsuccessfull_search)
			tail.selectIndices()
		}
		Trace.Printf("Sleep between queries: %d\n", delay)
		time.Sleep(delay)

		Trace.Printf("Query lastTimeStamp: %s\n", tail.lastTimeStamp)
		result, err := tail.executeSearch()

		if err != nil {
			Error.Fatalln("Error in executing search query.", err)
			return err
		}

		if result == nil || result.Hits == nil || result.Hits.Hits == nil {
			Info.Printf("Query: TotalHits EMPTY!!!")
			incomplete = false // exit now if we were just looking for stragglers
			unsuccessfull_search += 1
			if delay <= 5000*time.Millisecond {
				delay = delay + 500*time.Millisecond
				Trace.Printf("Delay query backoff: %d\n", delay)
			}
			continue
		} else {
			Trace.Printf("Query: TotalHits %d\n", len(result.Hits.Hits))
		}

		// output count and exit
		tail.processResults(result)
		if tail.count {
			tail.totalCount += len(result.Hits.Hits)
		}
		// push from date forward
		tail.queryDefinition.AfterDateTime = tail.lastTimeStamp

		//Dynamic delay calculation for determining delay between search requests
		if len(result.Hits.Hits) > 0 && delay > 500*time.Millisecond {
			delay = 500 * time.Millisecond
		} else if delay <= 5000*time.Millisecond {
			delay = delay + 500*time.Millisecond
			Trace.Printf("Delay query backoff: %d\n", delay)
		}
		if len(result.Hits.Hits) > 0 {
			unsuccessfull_search = 0
		} else {
			unsuccessfull_search += 1
		}

		// lets immediately do it again if we think this was an incomplete query - ie we hit the 10000 limit
		incomplete = false
		if len(result.Hits.Hits) == 10000 {
			incomplete = true
		}

		// tidy up unused connections
		if tail.httpClient != nil {
			tail.httpClient.CloseIdleConnections()
		}
	}
	// bracket the pretty printed results
	if tail.prettyPrint {
		fmt.Println("\n]")
	}
	if tail.count {
		fmt.Printf("%d\n", tail.totalCount)
		// os.Exit(0)
	}

	return nil
}

// Initial search needs to be run until we get at least one result
// in order to fetch the timestamp which we will use in subsequent follow searches
func (tail *Tail) executeSearch() (*elastic.SearchResult, error) {

	var json_query = ""
	var from_date = ""
	var to_date = ""
	var ids = ""
	if len(tail.lastIDs) > 0 {
		Trace.Printf("IDS: %v", tail.lastIDs)
		idsToFilter := make([]string, len(tail.lastIDs))
		for i := range tail.lastIDs {
			idsToFilter[i] = tail.lastIDs[i].id
		}
		ids = `"` + strings.Join(strings.Fields(fmt.Sprint(idsToFilter)), `","`) + `"`
	}
	ids = `[` + ids + `]`
	Trace.Printf("Excluding IDS: %d", len(ids))
	Trace.Printf("Formated IDS: %s", ids)
	if tail.queryDefinition.AfterDateTime == "" {
		from_date = "1970-01-01T00:00:00Z"
	} else {
		from_date = tail.queryDefinition.AfterDateTime
		// if this is not the first search, then step back a few seconds to give overlap
		if len(tail.lastIDs) > 0 {
			from_date = parseElasticTimeStamp(from_date).Add(-(time.Duration(5) * time.Second)).Format(time.RFC3339)
		}
	}
	if tail.queryDefinition.BeforeDateTime == "" {
		to_date = "now"
	} else {
		to_date = tail.queryDefinition.BeforeDateTime
	}

	var last_timestamp = tail.lastTimeStamp
	if last_timestamp == "" {
		last_timestamp = from_date // both are the from date really - need to rationalise - XXX
	}

	lts := parseElasticTimeStamp(last_timestamp)
	fts := parseElasticTimeStamp(from_date)
	// subtract 5 seconds so that we might pickup some missed records
	if lts.After(fts) {
		// from_date = last_timestamp
		from_date = fts.Add(-(time.Duration(5) * time.Second)).Format(time.RFC3339)
	}

	if len(tail.configuration.QueryDefinition.Terms) == 0 {

		if tail.configuration.QueryDefinition.AfterDateTime == "" && tail.configuration.QueryDefinition.BeforeDateTime == "" {
			Trace.Printf("Query is EMPTY  and no date range - match_all")
			json_query = fmt.Sprintf(`
			{
				"query" : {
					"bool" : {
						"must_not" : [
							{"terms": {"_id": %s}}
						],
						"filter" : [
							{
								"range" : {
									"@timestamp" : {
										"gte" : "%s"
									}
								}
							}
							]
					}
				},

				"size": %d,
				"sort":[{"%s":{"order":"desc"}}]
			}
	   `, ids, last_timestamp, tail.initialEntries, tail.queryDefinition.TimestampField)
		} else {
			Trace.Printf("Query is EMPTY  with date range ")
			json_query = fmt.Sprintf(`
			{
				"query" : {
					"bool" : {
						"must_not" : [
							{"terms": {"_id": %s}}
						],
						"must" : [
								{
									"range" : {
										"%s": {
											"gte" : "%s",
											"lte" : "%s"
										}
									}
								}
							]
					}
				},
				"size": %d,
				"sort":[{"%s":{"order":"desc"}}]
			}
			`, ids, tail.queryDefinition.TimestampField, from_date, to_date,
				tail.initialEntries, tail.queryDefinition.TimestampField)

		}
	} else {
		// terms are not empty
		if tail.configuration.QueryDefinition.AfterDateTime == "" && tail.configuration.QueryDefinition.BeforeDateTime == "" {
			// dates are empty
			Trace.Printf("Query is NOT empty and no date range - match_all")
			json_query = fmt.Sprintf(`
			{"from": 0,
				"query" : {
					"bool" : {
						"must_not" : [
							{"terms": {"_id": %s}}
						],
						"must" : [
								{ "query_string": {"query": "%s"} }
						],
						"filter" : [
							{
								"range" : {
									"@timestamp" : {
										"boost" : 1,
										"gte" : "%s"
									}
								}
							}
						]
					}
				},
				"size": %d,
				"sort":[{"%s":{"order":"desc"}}]
			}
	   `, ids, tail.buildQueryString(), last_timestamp, tail.initialEntries, tail.queryDefinition.TimestampField)

		} else {
			// dates are not empty
			Trace.Printf("Query is NOT EMPTY  with date range ")
			json_query = fmt.Sprintf(`
			{ "from": 0,
				"query" : {
					"bool" : {
						"must_not" : [
							{"terms": {"_id": %s}}
						],
						"must" : [
								{"range" : {
										"%s": {
											"gte" : "%s",
											"lte" : "%s"
										}
									}
								},
								{ "query_string": {"query": "%s"} }
						]
					}
				},
				"size": %d,
				"sort":[{"%s":{"order":"desc"}}]
			}
	   `, ids, tail.queryDefinition.TimestampField, from_date, to_date, tail.buildQueryString(), tail.initialEntries, tail.queryDefinition.TimestampField)

		}

	}

	Trace.Printf("Request Query: %s", json_query)

	var query_json map[string]interface{}
	err := json.Unmarshal([]byte(json_query), &query_json)
	if err != nil {
		Trace.Printf("ERR [Unmarshal Query] %s", err)
		panic(err)
	}
	json_query_bytes, err := json.Marshal(query_json)
	if err != nil {
		Error.Fatalf("Could not serialise query: %v", query_json)
	}

	indicies := findIndicesForDateRange(tail.indices, tail.configuration.SearchTarget.IndexPattern, from_date, to_date)
	Trace.Printf("Request Indicies: %s", indicies)
	var body = fmt.Sprintf("{\"index\":[\"%s\"]}\n%s\n", strings.Join(indicies, `","`), string(json_query_bytes))
	Trace.Printf("Request Query body: %s", body)

	// request for creating a token
	var req = request.AddApiKey(tail.configuration, request.BuildRequest(tail.configuration, "/_msearch", body, "POST"))
	Trace.Printf("Req: %v", req)

	resp, err := tail.httpClient.Do(req)
	if err != nil {
		Error.Fatalf("[query] http.Request failed: %s", err)
	}
	Trace.Printf("[query] Response: %v", resp)
	if resp.StatusCode != 200 {
		Error.Fatalf("[query] http.Request failed [%d]: %s", resp.StatusCode, resp.Status)
	}
	resp_body, err := io.ReadAll(resp.Body)
	if err != nil {
		Error.Fatalf("[query] http.Request body read failed: %s", err)
	}

	Trace.Printf("[query] Response body: %s", string(resp_body))

	// declaring the crazy struct
	var res map[string]interface{}
	err = json.Unmarshal([]byte(resp_body), &res)
	if err != nil {
		Error.Fatalf("[query] Response not a JSON object: %s/%s", string(body), err)
	}

	// Return result
	result := new(elastic.MultiSearchResult)
	decoder := &elastic.DefaultDecoder{}
	if err := decoder.Decode([]byte(resp_body), result); err != nil {
		Error.Fatalf("[query] Response not a JSON object: %s/%s", string(body), err)
		return nil, err
	}

	if len(result.Responses) > 0 && result.Responses[0].Hits != nil && result.Responses[0].Hits.Hits != nil {
		Info.Printf("executeSearch: (initialEntries: %d) TotalHits %d\n", tail.initialEntries, len(result.Responses[0].Hits.Hits))
		return result.Responses[0], nil
	} else {
		return nil, err
	}
}

// Process the results (e.g. prints them out based on configured format)
func (tail *Tail) processResults(searchResult *elastic.SearchResult) {
	if searchResult.Hits == nil {
		Error.Fatalln("Fatal error with searchResult.Hits. try -v3 to get details")
		return
	}

	hits := searchResult.Hits.Hits
	Trace.Printf("Fetched page of %d results.\n", len(hits))

	// We need to track last N entries that had the timestamp newer than cutoff timestamp. This is done to
	// avoid loosing entries that may have arrived to elasticsearch just as we were executing next query.
	// When tailing, we will
	// issue next query which will be filtered so that timestamps are greater or
	// equal to last timestamp minus tailing time window. Since we are tracking IDs of entries form previous query,
	// we can use the IDs to remove the duplicates. https://github.com/knes1/elktail/issues/11

	// XXX
	// slice results for n - then reverse row order unless truly descending

	// search results are in reverse order, so if descending just count off first N rows
	if tail.order {
		for i := 0; i < len(hits); i++ {
			hit := hits[i]
			de := displayedEntry{timeStamp: "", id: hit.Id}
			if !lastContains(tail.lastIDs, de) {
				entry := tail.processHit(hit)
				timeStamp := entry[tail.queryDefinition.TimestampField].(string)
				// need to find the max
				if timeStamp != tail.lastTimeStamp {
					ts := parseElasticTimeStamp(timeStamp)
					lts := parseElasticTimeStamp(tail.lastTimeStamp)
					if ts.After(lts) {
						tail.lastTimeStamp = timeStamp
					}
				}
				de := displayedEntry{timeStamp: timeStamp, id: hit.Id}
				if !lastContains(tail.lastIDs, de) {
					tail.lastIDs = append(tail.lastIDs, de)
				}
			}
		}

	} else { //when results are in ascending order, we need to process them in reverse
		// print all rows, because the timestamp and rowIDs added to the query
		// ensure that we only get the tail/follow on
		for i := len(hits) - 1; i >= 0; i-- {
			hit := hits[i]
			de := displayedEntry{timeStamp: "", id: hit.Id}
			if !lastContains(tail.lastIDs, de) {
				entry := tail.processHit(hit)
				timeStamp := entry[tail.queryDefinition.TimestampField].(string)
				// if timeStamp != tail.lastTimeStamp {
				ts := parseElasticTimeStamp(timeStamp)
				lts := parseElasticTimeStamp(tail.lastTimeStamp)
				if ts.After(lts) {
					tail.lastTimeStamp = timeStamp
				}
				// }
				de := displayedEntry{timeStamp: timeStamp, id: hit.Id}
				if !lastContains(tail.lastIDs, de) {
					tail.lastIDs = append(tail.lastIDs, de)
				}
			}
		}
	}
	tail.lastIDs = drainOldEntries(tail.lastIDs)

}

func lastContains(list []displayedEntry, x displayedEntry) bool {
	for _, item := range list {
		if item.id == x.id {
			return true
		}
	}
	return false
}

func parseElasticTimeStamp(elTimeStamp string) time.Time {
	timeStr, _ := time.Parse(dateFormatFull, elTimeStamp)
	return timeStr
}

func formatElasticTimeStamp(timeStamp time.Time) string {
	return timeStamp.Format(dateFormatFull)
}

func drainOldEntries(entries []displayedEntry) []displayedEntry {
	// only drain entries when we have more than a 10000
	// make sure they are sorted in date order and then shift from the front
	Trace.Println("draining: sorting")
	sort.Slice(entries, func(i, j int) bool {
		return entries[i].timeStamp < entries[j].timeStamp
	})
	Trace.Printf("draining: iterating %d \n", len(entries))
	for len(entries) > 10000 {
		i := (len(entries) - 10000)
		Trace.Printf("draining: removing %d \n", i)
		entries = entries[i:]
	}
	Trace.Printf("draining: drained %d \n", len(entries))
	return entries
}

func (tail *Tail) processHit(hit *elastic.SearchHit) map[string]interface{} {
	var entry map[string]interface{}
	err := json.Unmarshal(hit.Source, &entry)
	if err != nil {
		Error.Fatalln("Failed parsing ElasticSearch response.", err)
	}
	// short circuit if we are only counting
	if tail.count {
		return entry
	}

	// add the doc Id and timestamp
	if tail.addId {
		fmt.Printf("%s/%s"+color.Yellow+">"+color.Reset+" ", hit.Id, entry[tail.queryDefinition.TimestampField].(string))
	}

	if tail.prettyPrint {
		// 4 indent with 4 prefix
		bEntry, err := json.MarshalIndent(entry, "    ", "    ")
		if err != nil {
			Error.Fatalln("Failed to pretty print record ", err)
		} else {
			// print the , for the last record
			if !tail.first {
				fmt.Println(",")
			}
			// indent first line to match prefix of 4
			fmt.Print("    " + string(bEntry))
			tail.first = false
		}
	} else if tail.raw {
		fmt.Println(string(hit.Source))
	} else {
		tail.printResult(entry)
	}

	return entry
}

// Regexp for parsing out format fields
var formatJsonPathRegexp = regexp.MustCompile("\\{[^\\{]+\\}")
var tab = string("\t")

// Regexp for parsing out format fields
var formatRegexp = regexp.MustCompile("%[A-Za-z0-9@_.-\\/]+")

// Regexp for parsing out query terms
var queryTermsRegexp = regexp.MustCompile("[A-Za-z0-9_.-]+:")

// Print result according to format
func (tail *Tail) printResult(entry map[string]interface{}) {

	var queryTerms []string
	var terms string
	if tail.printTerms {
		// color.Yellow
		for _, f := range tail.queryDefinition.Terms {
			fields := queryTermsRegexp.FindAllString(f, -1)
			for _, q := range fields {
				if q != "message:" {
					queryTerms = append(queryTerms, strings.Trim(q, ":"))
				}
			}
		}
		var results []string
		for _, f := range queryTerms {
			value, _ := EvaluateExpression(entry, f)
			results = append(results, fmt.Sprintf("%s: %s", f, value))
		}
		terms = color.Yellow + "[" + strings.Join(results, " ") + "]> " + color.Reset
	}

	// find instances of message queries
	query := strings.Join(tail.queryDefinition.Terms, " ")
	var msgterms []string
	queryParts := regexp.MustCompile(`( and | or | AND | OR )`).Split(query, -1)
	for _, q := range queryParts {
		if !strings.Contains(q, ":") {
			msgterms = append(msgterms, q)
		} else if strings.Contains(q, "message:") {
			q = regexp.MustCompile(`message:\s+?`).ReplaceAllString(q, "")
			msgterms = append(msgterms, q)
		}
	}

	fields := formatRegexp.FindAllString(tail.queryDefinition.Format, -1)
	result := tail.queryDefinition.Format

	// enable highlighting in template
	bold := regexp.MustCompile(`\{bold\}`)
	result = bold.ReplaceAllString(result, color.Yellow)
	reset := regexp.MustCompile(`\{reset\}`)
	result = reset.ReplaceAllString(result, color.Reset)

	Trace.Printf("fields: %s", fields)
	for _, f := range fields {
		value, _ := EvaluateExpression(entry, f[1:])
		if len(msgterms) > 0 && tail.printTerms && f == "%message" {
			// find query string and bold
			for _, t := range msgterms {
				mt := regexp.MustCompile(`(?i)(` + t + `)`)
				value = mt.ReplaceAllString(value, color.Yellow+"$1"+color.Reset)
			}
		}

		// tab delimit output if requested
		if tail.delimiter && !tail.raw {
			result = strings.Replace(result, f, value+tab, -1)
		} else {
			result = strings.Replace(result, f, value, -1)

		}
	}

	// Now look for JSONPath expressions
	jpexps := formatJsonPathRegexp.FindAllString(tail.queryDefinition.Format, -1)
	Trace.Printf("JsonPath format: %s\n", tail.queryDefinition.Format)
	for _, jpexp := range jpexps {
		if jpexp == "{bold}" || jpexp == "{reset}" {
			continue
		}
		Trace.Printf("JsonPath term: %s\n", jpexp)
		term := strings.TrimSuffix(strings.TrimPrefix(jpexp, "{"), "}")
		if string(term[0]) == "." || string(term[0]) == "[" {
			term = "$" + term
		}

		var jpvalue string
		// parse .message to JSON if it is JSON
		if strings.HasPrefix(term, "$.message.") {
			var msg map[string]interface{}
			Info.Printf("message: %s\n", fmt.Sprint(entry["message"]))
			err := json.Unmarshal([]byte(fmt.Sprint(entry["message"])), &msg)
			// if we can't unpack JSON then it's a marshaling error
			if err != nil {
				if tail.IsVebose() {
					jpvalue = fmt.Sprintf("ERR [unmarshal message: %s] %s", fmt.Sprint(entry["message"]), err)
				} else {
					jpvalue = ""
				}
			} else {
				// else try the JsonPath expression requested
				msgterm := strings.TrimPrefix(term, "$.message")
				msgterm = "$" + msgterm
				value, err := jsonpath.Get(msgterm, msg)
				if err != nil {
					// only show the jsonpath error if in verbose mode else suppress
					if tail.IsVebose() {
						jpvalue = fmt.Sprintf("ERR [jsonpath: %s] %s", term, err)
					} else {
						jpvalue = ""
					}
				} else {
					jpvalue = fmt.Sprintf("%v", value)
				}
			}
		} else {
			// this is a non-message type jsonpath query
			value, err := jsonpath.Get(term, entry)
			if err != nil {
				// only show the jsonpath error if in verbose mode else suppress
				if tail.IsVebose() {
					jpvalue = fmt.Sprintf("ERR [jsonpath: %s] %s", term, err)
				} else {
					jpvalue = ""
				}
			} else {
				jpvalue = fmt.Sprintf("%v", value)
			}
		}
		// tab delimit output if requested
		if tail.delimiter && !tail.raw {
			result = strings.Replace(result, jpexp, jpvalue+tab, -1)
		} else {
			result = strings.Replace(result, jpexp, jpvalue, -1)
		}
	}

	// strip the last one back off again
	if tail.delimiter && !tail.raw {
		result = strings.TrimSuffix(result, tab)
	}
	if tail.lineno {
		tail.idx++
		fmt.Print(color.Green + "[" + fmt.Sprintf("%04d", tail.idx) + "] " + color.Reset)
	}
	fmt.Println(terms + result)
}

// EvaluateExpression Expression evaluation function. It uses map as a model and evaluates expression given as
// the parameter using dot syntax:
// "foo" evaluates to model[foo]
// "foo.bar" evaluates to model[foo][bar]
// If a key given in the expression does not exist in the model, function will return empty string and
// an error.
func EvaluateExpression(model interface{}, fieldExpression string) (string, error) {
	if fieldExpression == "" {
		return fmt.Sprintf("%v", model), nil
	}
	var nextModel interface{}
	nextExpression := ""
	modelMap, ok := model.(map[string]interface{})
	if ok {
		value := modelMap[fieldExpression]
		if value != nil {
			nextModel = value
		} else {
			parts := strings.SplitN(fieldExpression, ".", 2)
			expression := parts[0]
			if len(parts) > 1 {
				nextExpression = parts[1]
			}
			value = modelMap[expression]
			if value != nil { // *entries = (*entries)[i:]
				nextModel = value
			} else {
				return "", fmt.Errorf("Failed to evaluate expression %s on given model %+v (model map does not contain that key?)", fieldExpression, modelMap)
			}
		}
	} else {
		return "", fmt.Errorf("Model on which %s is to be evaluated is not a map", fieldExpression)
	}
	return EvaluateExpression(nextModel, nextExpression)
}

var queryEscape = regexp.MustCompile(`\\?/`)

func (tail *Tail) buildQueryString() string {
	if len(tail.queryDefinition.Terms) > 0 {
		result := strings.Join(tail.queryDefinition.Terms, " ")
		result = strings.ReplaceAll(result, " and ", " AND ")
		result = strings.ReplaceAll(result, " or ", " OR ")
		// double escape '/', but make sure it is not allreay escaped
		result = queryEscape.ReplaceAllString(result, "\\\\/")
		Trace.Printf("Running query string query: %s", result)
		return result
	} else {
		Trace.Print("Running query match all query.")
		return ""
	}

}

// Extracts and parses YMD date (year followed by month followed by day) from a given string. YMD values are separated by
// separator character given as argument.
func extractYMDDate(dateStr, separator string) time.Time {
	if dateStr == "now" {
		dateStr = time.Now().Format(time.RFC3339)
	}
	dateRegexp := regexp.MustCompile(fmt.Sprintf(`(\d{4}%s\d{2}%s\d{2})`, separator, separator))
	match := dateRegexp.FindAllStringSubmatch(dateStr, -1)
	if len(match) == 0 {
		Error.Fatalf("Failed to extract date: %s\n", dateStr)
	}
	result := match[0]
	parsed, err := time.Parse(fmt.Sprintf("2006%s01%s02", separator, separator), result[0])
	if err != nil {
		Error.Fatalf("Failed parsing date: %s", err)
	}
	return parsed
}

func findIndicesForDateRange(indices []string, indexPattern string, startDate string, endDate string) []string {
	start := extractYMDDate(startDate, "-")
	end := extractYMDDate(endDate, "-")
	Trace.Printf("Indicies start: %s/%s end: %s/%s", startDate, start, endDate, end)
	result := make([]string, 0, len(indices))
	for _, idx := range indices {
		Trace.Printf("findIndicesForDateRange: Match Idx: %v against: %v\n", idx, indexPattern)
		matched, _ := regexp.MatchString(indexPattern, idx)
		if matched {
			Trace.Printf("findIndicesForDateRange: Matched Idx: %v \n", idx)
			idxDate := extractYMDDate(idx, ".")
			// add a day because the indice may not arrive until the future
			end = end.Add(time.Duration(24) * time.Hour)
			Trace.Printf("findIndicesForDateRange: Idx Date: %v Start: %v - End: %v \n", idxDate, start, end)
			if (idxDate.After(start) || idxDate.Equal(start)) && (idxDate.Before(end) || idxDate.Equal(end)) {
				Trace.Printf("findIndicesForDateRange: Idx in date range: %v \n", idx)
				result = append(result, idx)
			}
		}
	}
	return result
}

func findLastIndex(indices []string, indexPattern string) string {
	var lastIdx string = ""
	date_part := "^" + strings.TrimSuffix(indexPattern, ".*") + "(.*)$"
	Trace.Printf("findLastIndex: regex for date %s", date_part)
	var date_part_regex = regexp.MustCompile(date_part)
	for _, idx := range indices {
		Trace.Printf("findLastIndex: Match Idx: %v against: %v\n", idx, indexPattern)
		matched, _ := regexp.MatchString(indexPattern, idx)
		if matched {
			idx_date_part := date_part_regex.ReplaceAllString(idx, `$1`)
			lastidx_date_part := date_part_regex.ReplaceAllString(lastIdx, `$1`)

			if lastIdx == "" {
				lastIdx = idx
			} else if idx_date_part > lastidx_date_part {
				lastIdx = idx
			}
		}
	}
	return lastIdx
}

// Must is a helper function to avoid boilerplate error handling for regex matches
// this way they may be used in single value context
func Must(result bool, err error) bool {
	if err != nil {
		Error.Panic(err)
	}
	return result
}

type StatusResponse struct {
	version VersionResponse
}

type VersionResponse struct {
	number string
}

func ExtractHeader(s string) []string {
	split := strings.Split(s, ":")
	for k, v := range split {
		split[k] = strings.Trim(v, " ")
	}
	return split
}
